# EatOnWeb

## REST service for EatOn application


# Libs
 * Slim [http://www.slimframework.com/]
 * Pusher [https://pusher.com/]


# Run
 If you use XAMPP put the app in htdocs folder, start the Apache server and you can see the app on the ```localhost/EatOn/public/server/```


