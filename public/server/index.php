<?php
            
namespace api;

require "../../vendor/autoload.php";

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use extension\ErrorHandler;
use extension\NotFoundErrorHandler;
use shared\QueryOptions as QueryOptions;

//Awardspace hita warninge
error_reporting(E_ERROR | E_PARSE);

$configuration = [
    "settings" => [
        "determineRouteBeforeAppMiddleware" => true,
        "displayErrorDetails" => true
    ],
];

$c = new \Slim\Container($configuration);

$app = new \Slim\App($c);

$c = $app->getContainer();
$c["errorHandler"] = function ($c) { return new ErrorHandler(); };
$c["notFoundHandler"] = function ($c) { return new NotFoundErrorHandler(); };



/* Batch request controller stuff */
$c["app"] = function ($c) {  global $app; return $app;};
$c["controller\BatchController"] = function ($c) {return new \controller\BatchController($c, $c["app"]); };


//Da se na svaki "options" request odgovori bar sa necin (da se ne desava "not found" greska)
$app->options("/{routes:.+}", function ($request, $response, $args) {
    return $response;
});

//Na svaki request dodaj određene hedere
$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader("Access-Control-Allow-Origin", "*")
            //->withHeader("Access-Control-Expose-Headers", "*")
            ->withHeader("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, Accept, Origin, Authorization, App-User-Is-Company")
            ->withHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, PATCH, OPTIONS")
            ->withHeader("Content-Type", "application/json");
});


//Popis dostupnih ruta
$app->get("/available", function (Request $request, Response $response, array $args) use ($app) {
   
    //$response->getBody()->write("test works");
    
    $routes = $app->getContainer()->get('router')->getRoutes();
    
    foreach ($routes as $r){
       /* @var $r \Slim\Route */
       
       $methods = "";
       foreach ($r->getMethods() as $m){
            if($methods != "")
                $methods .= ", ";
            
            $methods .= $m;
       }
        
        echo "[". $methods ."] ". $r->getPattern() . PHP_EOL;
       
        //var_dump($r->getMethods());
    }
    
    
     return $response;
});


/* Routes */

use controller\UserController as UserController;
$app->get("/users", UserController::class . ":getAll");
$app->get("/users/{id:[0-9]+}", UserController::class . ":get");
$app->post("/users", UserController::class . ":add");
$app->patch("/users/{id:[0-9]+}", UserController::class . ":update");
$app->delete("/users/{id:[0-9]+}", UserController::class . ":delete");


use controller\CompanyController as CompanyController;
$app->get("/companies", CompanyController::class . ":getAll");
$app->get("/companies/{id:[0-9]+}", CompanyController::class . ":get");
$app->post("/companies", CompanyController::class . ":add");
$app->patch("/companies/{id:[0-9]+}", CompanyController::class . ":update");
$app->delete("/companies/{id:[0-9]+}", CompanyController::class . ":delete");

$ohp = "/companies/{companyId:[0-9]+}/openingHours";
use controller\OpeningHourController as OpeningHourController;
$app->get($ohp, OpeningHourController::class . ":getAll");
$app->get($ohp."/{id:[0-9]+}", OpeningHourController::class . ":get");
$app->post($ohp, OpeningHourController::class . ":add");
$app->patch($ohp."/{id:[0-9]+}", OpeningHourController::class . ":update");
$app->delete($ohp."/{id:[0-9]+}", OpeningHourController::class . ":delete");


$mp = "/companies/{companyId:[0-9]+}/menus";
use controller\MenuController as MenuController;
$app->get($mp, MenuController::class . ":getAll");
$app->get($mp."/{id:[0-9]+}", MenuController::class . ":get");
$app->post($mp, MenuController::class . ":add");
$app->patch($mp."/{id:[0-9]+}", MenuController::class . ":update");
$app->delete($mp."/{id:[0-9]+}", MenuController::class . ":delete");


$mip = "/companies/{companyId:[0-9]+}/menus/{menuId:[0-9]+}/menuItems";
use controller\MenuItemController as MenuItemController;
$app->get($mip, MenuItemController::class . ":getAll");
$app->get($mip."/{id:[0-9]+}", MenuItemController::class . ":get");
$app->post($mip, MenuItemController::class . ":add");
$app->patch($mip."/{id:[0-9]+}", MenuItemController::class . ":update");
$app->delete($mip."/{id:[0-9]+}", MenuItemController::class . ":delete");


$miop = "/companies/{companyId:[0-9]+}/menus/{menuId:[0-9]+}/menuItems/{menuItemId:[0-9]+}/menuItemOptions";
use controller\MenuItemOptionController as MenuItemOptionController;
$app->get($miop, MenuItemOptionController::class . ":getAll");
$app->get($miop."/{id:[0-9]+}", MenuItemOptionController::class . ":get");
$app->post($miop, MenuItemOptionController::class . ":add");
$app->patch($miop."/{id:[0-9]+}", MenuItemOptionController::class . ":update");
$app->delete($miop."/{id:[0-9]+}", MenuItemOptionController::class . ":delete");


$miosp = "/companies/{companyId:[0-9]+}/menus/{menuId:[0-9]+}/menuItems/{menuItemId:[0-9]+}/menuItemOptions/{menuItemOptionId:[0-9]+}/menuItemOptionSupplements";
use controller\MenuItemOptionSupplementController as MenuItemOptionSupplementController;
$app->get($miosp, MenuItemOptionSupplementController::class . ":getAll");
$app->get($miosp."/{id:[0-9]+}", MenuItemOptionSupplementController::class . ":get");
$app->post($miosp, MenuItemOptionSupplementController::class . ":add");
$app->patch($miosp."/{id:[0-9]+}", MenuItemOptionSupplementController::class . ":update");
$app->delete($miosp."/{id:[0-9]+}", MenuItemOptionSupplementController::class . ":delete");

/*
use controller\ReservationController as ReservationController;
$app->get("/reservation", ReservationController::class . ":getAll");
$app->get("/reservation/{id:[0-9]+}", ReservationController::class . ":get");
$app->post("/reservation", ReservationController::class . ":add");
$app->patch("/reservation/{id:[0-9]+}", ReservationController::class . ":update");
$app->delete("/reservation/{id:[0-9]+}", ReservationController::class . ":delete");
*/

$op = "/orders";
use controller\OrderController as OrderController;
$app->get($op, OrderController::class . ":getAll");
$app->get($op."/{id:[0-9]+}", OrderController::class . ":get");
$app->get($op."/{orderId:[0-9]+}/user/{id:[0-9]+}", OrderController::class . ":getUser");
$app->post($op, OrderController::class . ":add");
$app->patch($op."/{id:[0-9]+}", OrderController::class . ":update");
$app->delete($op."/{id:[0-9]+}", OrderController::class . ":delete");


$oip = "/orders/{orderId:[0-9]+}/orderItems";
use controller\OrderItemController as OrderItemController;
$app->get($oip, OrderItemController::class . ":getAll");
$app->get($oip."/{id:[0-9]+}", OrderItemController::class . ":get");
$app->post($oip, OrderItemController::class . ":add");
$app->patch($oip."/{id:[0-9]+}", OrderItemController::class . ":update");
$app->delete($oip."/{id:[0-9]+}", OrderItemController::class . ":delete");

$oisp = "/orders/{orderId:[0-9]+}/orderItems/{orderItemId:[0-9]+}/orderItemSupplements";
use controller\OrderItemSupplementController as OrderItemSupplementController;
$app->get($oisp, OrderItemSupplementController::class . ":getAll");
$app->get($oisp."/{id:[0-9]+}", OrderItemSupplementController::class . ":get");
$app->post($oisp, OrderItemSupplementController::class . ":add");
$app->patch($oisp."/{id:[0-9]+}", OrderItemSupplementController::class . ":update");
$app->delete($oisp."/{id:[0-9]+}", OrderItemSupplementController::class . ":delete");


use controller\StatisticsController as StatisticsController;
$app->get("/statistics/annually", StatisticsController::class . ":getAnnually");
$app->get("/statistics/monthlyProducts/{month:[0-9]+}", StatisticsController::class . ":getMonthlyProducts");



use controller\BatchController as BatchController;
$app->post("/batch", BatchController::class . ":process");

use controller\AuthController as AuthController;
$app->post("/auth", AuthController::class . ":auth");
$app->post("/auth2", AuthController::class . ":auth2");


use controller\UploadController as UploadController;
$app->post("/file/upload", UploadController::class . ":upload");


/* End Routes */


//Ako se ne uspije nać pravi handler za request onda request obradi pomoću notFountHandlera
$app->map(["GET", "POST", "PUT", "DELETE", "PATCH"], "/{routes:.+}", function($req, $res) {
    $handler = $this->notFoundHandler; // handle using the default Slim page not found handler
    return $handler($req, $res);
});


//Pokreni aplikaciju
$app->run();


