<?php

namespace db;

use model\OrderItemSupplement as OrderItemSupplement;
use \shared\QueryOptions as QueryOptions;
use \mysqli as mysqli;

class DBOrderItemSupplement extends DBBase {

    const ATTRIBUTES = "id, order_item_id, menu_item_option_supplement_id";
    const BIND_TYPES = "iii";
    const TABLE_NAME = \config\DB::ORDER_ITEM_SUPPLEMENT_TABLE;//"order_item_supplement";

    public static function GetAttributes() {
        return self::ATTRIBUTES;
    }

    public static function GetBindTypes() {
        return self::BIND_TYPES;
    }

    public static function GetTableName() {
        return self::TABLE_NAME;
    }

    public static function GetAll(mysqli $db, QueryOptions $qo) {

        $items = array();
        //$query = "SELECT id, order_item_id, menu_item_supplement_id FROM order_item_supplement";
        //$query = "SELECT " . self::GetAttributes() . " FROM order_item_supplement";
        $query = "SELECT item.id, item.order_item_id, item.menu_item_option_supplement_id, sup.name, sup.price
                    FROM ".self::GetTableName()." item, ".\db\DBMenuItemOptionSupplement::GetTableName()." sup 
                    WHERE sup.id = item.menu_item_option_supplement_id";
        
        if($qo->Has(QueryOptions::ORDER_ITEM_ID))
            $query .= " AND order_item_id = " . $qo->GetOrderItemId();
        
        if ($stmt = $db->prepare($query)) {
            
            $stmt->execute();
            $stmt->bind_result($id, $orderItemId, $menuItemSupplementId, $name, $price);

            while ($stmt->fetch()) {

                try {
                    array_push($items, new OrderItemSupplement($id, $orderItemId, $menuItemSupplementId, $name, $price));
                } catch (Exception $e) {
                    self::processException($e);
                }
            }

            $stmt->close();
        }

        return $items;
    }

    public static function GetById(mysqli $db, QueryOptions $qo) {

        $item = null;
        //$query = "SELECT id, order_item_id, menu_item_supplement_id FROM order_item_supplement WHERE id = ?";
        //$query = "SELECT " . self::GetAttributes() . " FROM order_item_supplement WHERE id = ?";
        $query = "SELECT item.id, item.order_item_id, item.menu_item_option_supplement_id, sup.name, sup.price
                            FROM ".self::GetTableName()." item, ".\db\DBMenuItemOptionSupplement::GetTableName()." sup
                            WHERE sup.id = item.menu_item_option_supplement_id AND item.id = ?";
        
        if ($stmt = $db->prepare($query)) {

            $stmt->bind_param("i", $qo->GetId());
            $stmt->execute();
            $stmt->bind_result($id, $orderItemId, $menuItemSupplementId, $name, $price);

            if ($stmt->fetch()) {

                try {

                    $item = new OrderItemSupplement($id, $orderItemId, $menuItemSupplementId, $name, $price);
                } catch (Exception $e) {
                    self::processException($e);
                }
            }

            $stmt->close();
        }

        return $item;
    }

    protected static function bindInsertStatement($stmt, $item, $user) {
        $stmt->bind_param(self::GetBindTypes(), /*$item->GetId()*/$id=NULL, $item->GetOrderItemId(), $item->GetMenuItemSupplementId());
    }

    protected static function bindUpdateStatement($stmt, $item, $user) {
        $stmt->bind_param(self::GetBindTypes() . "i", $item->GetId(), $item->GetOrderItemId(), $item->GetMenuItemSupplementId(), $item->GetId());
    }

    //protected static function buildUpdateStatement(){
    //  return "UPDATE order_item_supplement SET id = ?, order_item_id = ?, menu_item_supplement_id = ? WHERE id = ?";
    //}
}
