<?php

namespace db;

use model\MenuItem as MenuItem;
use \shared\QueryOptions as QueryOptions;
use \mysqli as mysqli;

class DBMenuItem extends DBBase {

    const ATTRIBUTES = "id, menu_id, name, price, rate, photo, discount, description";
    const BIND_TYPES = "iisiisis";
    const TABLE_NAME = \config\DB::MENU_ITEM_TABLE;//"menu_item";

    public static function GetAttributes() {
        return self::ATTRIBUTES;
    }

    public static function GetBindTypes() {
        return self::BIND_TYPES;
    }

    public static function GetTableName() {
        return self::TABLE_NAME;
    }

    public static function GetAll(mysqli $db, QueryOptions $qo) {

        $items = array();
        //$query = "SELECT id, menu_id, name, price, rate, photo, discount, description FROM menu_item";
        $query = "SELECT " . self::GetAttributes() . " FROM ".self::GetTableName();
        
        if ($qo->Has(QueryOptions::MENU_ID))
            $query .= " WHERE menu_id = ".$qo->GetMenuId();

        if ($stmt = $db->prepare($query)) {
            $stmt->execute();
            $stmt->bind_result($id, $menuId, $name, $price, $rate, $photo, $discount, $description);

            while ($stmt->fetch()) {

                try {
                    array_push($items, new MenuItem($id, $menuId, $name, $price, $rate, $photo, $discount, $description));
                } catch (Exception $e) {
                    self::processException($e);
                }
            }

            $stmt->close();
        }

        return $items;
    }

    public static function GetById(mysqli $db, QueryOptions $qo) {

        $item = null;
        //$query = "SELECT id, menu_id, name, price, rate, photo, discount, description FROM menu_item WHERE id = ?";
        $query = "SELECT " . self::GetAttributes() . " FROM ".self::GetTableName()." WHERE id = ?";

        if ($stmt = $db->prepare($query)) {

            $stmt->bind_param("i", $qo->GetId());
            $stmt->execute();
            $stmt->bind_result($id, $menuId, $name, $price, $rate, $photo, $discount, $description);

            if ($stmt->fetch()) {

                try {

                    $item = new MenuItem($id, $menuId, $name, $price, $rate, $photo, $discount, $description);
                } catch (Exception $e) {
                    self::processException($e);
                }
            }

            $stmt->close();
        }

        return $item;
    }

    protected static function bindInsertStatement($stmt, $item, $user) {
        $stmt->bind_param(self::GetBindTypes(), /*$item->GetId()*/$id=NULL, $item->GetMenuId(), $item->GetName(), $item->GetPrice(), $item->GetRate(), $item->GetPhoto(), $item->GetDiscount(), $item->GetDescription());
    }

    protected static function bindUpdateStatement($stmt, $item, $user) {
        $stmt->bind_param(self::GetBindTypes() . "i", $item->GetId(), $item->GetMenuId(), $item->GetName(), $item->GetPrice(), $item->GetRate(), $item->GetPhoto(), $item->GetDiscount(), $item->GetDescription(), $item->GetId());
    }

    //protected static function buildUpdateStatement(){
    //  return "UPDATE menu_item SET id = ?, menu_id = ?, name = ?, price = ?, rate = ?, photo = ?, discount = ?, description = ? WHERE id = ?";
    //}
}
