<?php


namespace db;
use \mysqli as mysqli;

class DBBase {

    public static function GetTableName(){return "";}
    
    protected static function generateQuestionMarks(){
        $c = get_called_class();
        $len = strlen($c::GetBindTypes());
        $vals = "";
        
        for($i=0; $i<$len; $i++){
            $vals .= "?, ";
        }
        
        $vals = substr($vals, 0, strlen($vals)-2);
       
        return $vals;
    }

    
    
 
    protected static function buildInsertStatement(){
        $c = get_called_class();
        return "INSERT INTO ".$c::GetTableName()."(".$c::GetAttributes().") VALUES(".$c::generateQuestionMarks().")";
    }
    
    protected static function bindInsertStatement($stmt, $item, $user){
      $stmt->bind_param("i", $item->GetId());
    }


    public static function Save(mysqli $db, $item, $user){
      $c = get_called_class();
        
      $stmt = $db->prepare($c::buildInsertStatement());
      $c::bindInsertStatement($stmt, $item, $user);
      
      try{

        $stmt->execute();
        $item->SetId($db->insert_id);
        $stmt->close();

      }catch(Exception $e){
        self::processException($e);
      }          
        
    }

    


    
    protected static function buildUpdateStatement(){
        $c = get_called_class();
        
        $s = "UPDATE ".$c::GetTableName()." SET ";
        
        $s .= str_replace(", ", " = ?, ", $c::GetAttributes());
        
        $s .= " = ? WHERE id = ?";
        
        return $s;
    }
    
    protected static function bindUpdateStatement($stmt, $item, $user){
      $stmt->bind_param("i", $item->GetId());
    }
    
    public static function Update(mysqli $db, $item, $user){
      $c = get_called_class();
     
      $stmt = $db->prepare($c::buildUpdateStatement());
      $c::bindUpdateStatement($stmt, $item, $user);
              
      if ($stmt->execute() === false) {
            self::processStmtException($stmt);
      }

      $stmt->close();
    }
    
    
    
    protected static function buildDeleteStatement(\shared\QueryOptions $qo){
         $c = get_called_class();
        
        return "DELETE FROM ".$c::GetTableName()." WHERE id = ?";
    }
    
    
    //public static function Delete(mysqli $db, $id, $user){
    public static function Delete(mysqli $db, \shared\QueryOptions $qo, $user){
        $c = get_called_class();
         
        $stmt = $db->prepare($c::buildDeleteStatement($qo));
        $stmt->bind_param("i", $qo->GetId());

        if ($stmt->execute() === false) {
            self::processStmtException($stmt);
        }
        $stmt->close();
    }
    
    
    
    
    
    
    protected static function processStmtException(\mysqli_stmt $stmt){
       
        //var_dump($stmt);
        //$errorMsg = "Greška kod ažuriranja podataka! Detalji: " . $stmt->error;
        $errorMsg = $stmt->error;
        
        $stmt->close();
       
        throw new \Exception($errorMsg);
    }
    
    protected static function processException($e){
        throw $e;
    }
}
