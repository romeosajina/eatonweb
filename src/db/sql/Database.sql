create database EatOn;
use EatOn;
 ALTER SCHEMA `eaton`  DEFAULT CHARACTER SET utf8 ;

CREATE TABLE `user` (
    id INTEGER NOT NULL AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL,
    surname VARCHAR(50) NOT NULL,
    city VARCHAR(50) NOT NULL,
    address VARCHAR(50) NOT NULL,
    latitude DECIMAL(10,7) NOT NULL,
    longitude DECIMAL(10,7) NOT NULL,
    phone VARCHAR(50),
    show_number BOOLEAN NOT NULL,
    email VARCHAR(200) NOT NULL UNIQUE,
    `password` VARCHAR(200) NOT NULL,
    CONSTRAINT CHECK (show_number IN (0, 1)),
    CONSTRAINT user_id_pk PRIMARY KEY (id)
);
CREATE TABLE company (
    id INTEGER NOT NULL AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL,
    city VARCHAR(50) NOT NULL,
    address VARCHAR(50) NOT NULL,
    latitude DECIMAL(10,7) NOT NULL,
    longitude DECIMAL(10,7) NOT NULL,
    photo VARCHAR(300) NOT NULL,
    phone VARCHAR(50) NOT NULL,
    delivery VARCHAR(50) NOT NULL,
    pick_up VARCHAR(50) NOT NULL,
    min_delivery_price DECIMAL(8 , 2 ),
    max_destination_range DECIMAL(6 , 2 ),
    max_free_delivery_range DECIMAL(6 , 2 ),
    description VARCHAR(1000),
    email VARCHAR(200) NOT NULL UNIQUE,
    `password` VARCHAR(500) NOT NULL,
    eth_address VARCHAR(50),
    CONSTRAINT company_id_pk PRIMARY KEY (id)
);
CREATE TABLE opening_hour (
    id INTEGER NOT NULL AUTO_INCREMENT,
    company_id INTEGER NOT NULL,
    day INTEGER NOT NULL,
    `from` TIMESTAMP NOT NULL,
    `to` TIMESTAMP NOT NULL,
    CONSTRAINT opening_hour_company_id_fk FOREIGN KEY (company_id)
        REFERENCES company (id),
    CONSTRAINT opening_hour_id_pk PRIMARY KEY (id)
);
CREATE TABLE menu (
    id INTEGER NOT NULL AUTO_INCREMENT,
    company_id INTEGER NOT NULL,
    type VARCHAR(50) NOT NULL,
    photo VARCHAR(300),
    CONSTRAINT menu_company_id_fk FOREIGN KEY (company_id)
        REFERENCES company (id),
    CONSTRAINT menu_id_pk PRIMARY KEY (id)
);
CREATE TABLE menu_item (
    id INTEGER NOT NULL AUTO_INCREMENT,
    menu_id INTEGER NOT NULL,
    name VARCHAR(50) NOT NULL,
    price DECIMAL(8 , 2 ) NOT NULL,
    rate DECIMAL(5 , 2 ) NOT NULL,
    photo VARCHAR(300),
    discount DECIMAL(8 , 2 ),
    description VARCHAR(50),
    CONSTRAINT menu_item_menu_id_fk FOREIGN KEY (menu_id)
        REFERENCES menu (id),
    CONSTRAINT menu_item_id_pk PRIMARY KEY (id)
);
CREATE TABLE menu_item_option (
    id INTEGER NOT NULL AUTO_INCREMENT,
    menu_item_id INTEGER NOT NULL,
    name VARCHAR(50) NOT NULL,
    price DECIMAL(8 , 2 ) NOT NULL,
    CONSTRAINT menu_item_option_menu_item_id_fk FOREIGN KEY (menu_item_id)
        REFERENCES menu_item (id),
    CONSTRAINT menu_item_option_id_pk PRIMARY KEY (id)
);
CREATE TABLE menu_item_option_supplement (
    id INTEGER NOT NULL AUTO_INCREMENT,
    menu_item_option_id INTEGER NOT NULL,
    name VARCHAR(50) NOT NULL,
    price DECIMAL(8 , 2 ) NOT NULL,
    quantity INTEGER NOT NULL,
    photo VARCHAR(300),
    CONSTRAINT menu_item_option_supplement_menu_item_option_id_fk FOREIGN KEY (menu_item_option_id)
        REFERENCES menu_item_option (id),
    CONSTRAINT menu_item_option_supplement_id_pk PRIMARY KEY (id)
);
CREATE TABLE reservation (
    id INTEGER NOT NULL AUTO_INCREMENT,
    user_id INTEGER NOT NULL,
    company_id INTEGER NOT NULL,
    date TIMESTAMP NOT NULL,
    status VARCHAR(50) NOT NULL,
    remark VARCHAR(50),
    CONSTRAINT reservation_user_id_fk FOREIGN KEY (user_id)
        REFERENCES user (id),
    CONSTRAINT reservation_company_id_fk FOREIGN KEY (company_id)
        REFERENCES company (id),
    CONSTRAINT reservation_id_pk PRIMARY KEY (id)
);
CREATE TABLE `order` (
    id INTEGER NOT NULL AUTO_INCREMENT,
    user_id INTEGER NOT NULL,
    company_id INTEGER NOT NULL,
    date TIMESTAMP NOT NULL,
    delivery_time TIMESTAMP NOT NULL,
    status VARCHAR(50) NOT NULL,
    rate INTEGER NOT NULL,
    remark VARCHAR(50),
    payment_method VARCHAR(1) DEFAULT 'C',
    CONSTRAINT payment_method IN ('E', 'C'),
    CONSTRAINT order_user_id_fk FOREIGN KEY (user_id)
        REFERENCES user (id),
    CONSTRAINT order_company_id_fk FOREIGN KEY (company_id)
        REFERENCES company (id),
    CONSTRAINT order_id_pk PRIMARY KEY (id)
);
CREATE TABLE order_item (
    id INTEGER NOT NULL AUTO_INCREMENT,
    order_id INTEGER NOT NULL,
    menu_item_id INTEGER NOT NULL,
    menu_item_option_id INTEGER NOT NULL,
    amount INTEGER NOT NULL,
    discount DECIMAL(5 , 2 ),
    remark VARCHAR(50),
    CONSTRAINT order_item_menu_item_id_fk FOREIGN KEY (menu_item_id)
        REFERENCES menu_item (id),
    CONSTRAINT order_item_menu_item_option_id_fk FOREIGN KEY (menu_item_option_id)
        REFERENCES menu_item_option (id),
    CONSTRAINT order_item_order_id_option_id_fk FOREIGN KEY (order_id)
        REFERENCES `order` (id),
    CONSTRAINT order_item_id_pk PRIMARY KEY (id)
);
CREATE TABLE order_item_supplement (
    id INTEGER NOT NULL AUTO_INCREMENT,
    order_item_id INTEGER NOT NULL,
    menu_item_option_supplement_id INTEGER NOT NULL,
    CONSTRAINT order_item_supplement_order_item_id_fk FOREIGN KEY (order_item_id) REFERENCES order_item (id),
    CONSTRAINT order_item_supplement_menu_item_supplement_id_fk FOREIGN KEY (menu_item_option_supplement_id) REFERENCES menu_item_option_supplement (id),
    CONSTRAINT order_item_supplement_id_pk PRIMARY KEY (id)
);


alter table company default charset=utf8 collate utf8_croatian_ci;
alter table menu default charset=utf8 collate utf8_croatian_ci;
alter table menu_item default charset=utf8 collate utf8_croatian_ci;
alter table menu_item_option default charset=utf8 collate utf8_croatian_ci;
alter table menu_item_option_supplement default charset=utf8 collate utf8_croatian_ci;
alter table opening_hour default charset=utf8 collate utf8_croatian_ci;
alter table `order` default charset=utf8 collate utf8_croatian_ci;
alter table order_item default charset=utf8 collate utf8_croatian_ci;
alter table order_item_supplement default charset=utf8 collate utf8_croatian_ci;
alter table reservation default charset=utf8 collate utf8_croatian_ci;
alter table `user` default charset=utf8 collate utf8_croatian_ci;



UPDATE `order` SET `date` = '2018-01-13 07:30:33' WHERE `order`.`id` = 19;
UPDATE `order` SET `date` = '2018-02-13 07:30:33' WHERE `order`.`id` = 20;
UPDATE `order` SET `date` = '2018-03-13 07:30:33' WHERE `order`.`id` = 21;
UPDATE `order` SET `date` = '2018-04-13 07:30:33' WHERE `order`.`id` = 22;
UPDATE `order` SET `date` = '2018-05-13 07:30:33' WHERE `order`.`id` = 23;
UPDATE `order` SET `date` = '2018-06-13 07:30:33' WHERE `order`.`id` = 24;
UPDATE `order` SET `date` = '2018-07-13 07:30:33' WHERE `order`.`id` = 25;
UPDATE `order` SET `date` = '2018-08-13 07:30:33' WHERE `order`.`id` = 26;
UPDATE `order` SET `date` = '2018-09-13 07:30:33' WHERE `order`.`id` = 27;
UPDATE `order` SET `date` = '2018-10-13 07:30:33' WHERE `order`.`id` = 28;
UPDATE `order` SET `date` = '2018-11-13 07:30:33' WHERE `order`.`id` = 29;
UPDATE `order` SET `date` = '2018-12-13 07:30:33' WHERE `order`.`id` = 30;
UPDATE `order` SET `date` = '2019-01-13 07:30:33' WHERE `order`.`id` = 31;
UPDATE `order` SET `date` = '2019-02-13 07:30:33' WHERE `order`.`id` = 32;
UPDATE `order` SET `date` = '2019-03-13 07:30:33' WHERE `order`.`id` = 33;
UPDATE `order` SET `date` = '2019-04-13 07:30:33' WHERE `order`.`id` = 34;
UPDATE `order` SET `date` = '2019-05-13 07:30:33' WHERE `order`.`id` = 35;
UPDATE `order` SET `date` = '2019-06-13 07:30:33' WHERE `order`.`id` = 36;
UPDATE `order` SET `date` = '2019-07-13 07:30:33' WHERE `order`.`id` = 37;
UPDATE `order` SET `date` = '2019-08-13 07:30:33' WHERE `order`.`id` = 38;
UPDATE `order` SET `date` = '2019-09-13 07:30:33' WHERE `order`.`id` = 39;
UPDATE `order` SET `date` = '2019-10-13 07:30:33' WHERE `order`.`id` = 40;
UPDATE `order` SET `date` = '2019-11-13 07:30:33' WHERE `order`.`id` = 41;
UPDATE `order` SET `date` = '2019-12-13 07:30:33' WHERE `order`.`id` = 42;
UPDATE `order` SET `date` = '2017-01-13 07:30:33' WHERE `order`.`id` = 43;
UPDATE `order` SET `date` = '2017-02-13 07:30:33' WHERE `order`.`id` = 44;
UPDATE `order` SET `date` = '2017-03-13 07:30:33' WHERE `order`.`id` = 45;
UPDATE `order` SET `date` = '2017-04-13 07:30:33' WHERE `order`.`id` = 46;
UPDATE `order` SET `date` = '2017-05-13 07:30:33' WHERE `order`.`id` = 47;
UPDATE `order` SET `date` = '2017-06-13 07:30:33' WHERE `order`.`id` = 48;
UPDATE `order` SET `date` = '2017-07-13 07:30:33' WHERE `order`.`id` = 50;
UPDATE `order` SET `date` = '2017-08-13 07:30:33' WHERE `order`.`id` = 51;
UPDATE `order` SET `date` = '2017-09-13 07:30:33' WHERE `order`.`id` = 52;
UPDATE `order` SET `date` = '2017-10-13 07:30:33' WHERE `order`.`id` = 53;
UPDATE `order` SET `date` = '2017-11-13 07:30:33' WHERE `order`.`id` = 54;
UPDATE `order` SET `date` = '2017-12-13 07:30:33' WHERE `order`.`id` = 55;

UPDATE `order` SET delivery_time = `date`+ INTERVAL 30 MINUTE


/*
ALTER TABLE `eaton`.`company` 
ADD COLUMN `eth_address` VARCHAR(50) NULL AFTER `password`;

ALTER TABLE `eaton`.`order` 
ADD COLUMN `payment_method` VARCHAR(1) NULL DEFAULT 'C' AFTER `remark`;

*/