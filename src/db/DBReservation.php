<?php

namespace db;

use model\Reservation as Reservation;
use \shared\QueryOptions as QueryOptions;
use \mysqli as mysqli;

class DBReservation extends DBBase {

    const ATTRIBUTES = "id, user_id, company_id, date, status, remark";
    const BIND_TYPES = "iiisss";
    const TABLE_NAME = \config\DB::RESERVATION_TABLE;//"reservation";

    public static function GetAttributes() {
        return self::ATTRIBUTES;
    }

    public static function GetBindTypes() {
        return self::BIND_TYPES;
    }

    public static function GetTableName() {
        return self::TABLE_NAME;
    }

    public static function GetAll(mysqli $db, QueryOptions $qo) {

        $items = array();
        //$query = "SELECT id, user_id, company_id, date, status, remark FROM reservation";
        $query = "SELECT " . self::GetAttributes() . " FROM ".self::GetTableName();

        if ($stmt = $db->prepare($query)) {
            $stmt->execute();
            $stmt->bind_result($id, $userId, $companyId, $date, $status, $remark);

            while ($stmt->fetch()) {

                try {
                    array_push($items, new Reservation($id, $userId, $companyId, $date, $status, $remark));
                } catch (Exception $e) {
                    self::processException($e);
                }
            }

            $stmt->close();
        }

        return $items;
    }

    public static function GetById(mysqli $db, QueryOptions $qo) {

        $item = null;
        //$query = "SELECT id, user_id, company_id, date, status, remark FROM reservation WHERE id = ?";
        $query = "SELECT " . self::GetAttributes() . " FROM ".self::GetTableName()." WHERE id = ?";

        if ($stmt = $db->prepare($query)) {

            $stmt->bind_param("i", $qo->GetId());
            $stmt->execute();
            $stmt->bind_result($id, $userId, $companyId, $date, $status, $remark);

            if ($stmt->fetch()) {

                try {

                    $item = new Reservation($id, $userId, $companyId, $date, $status, $remark);
                } catch (Exception $e) {
                    self::processException($e);
                }
            }

            $stmt->close();
        }

        return $item;
    }

    protected static function bindInsertStatement($stmt, $item, $user) {
        $stmt->bind_param(self::GetBindTypes(), /*$item->GetId()*/$id=NULL, $item->GetUserId(), $item->GetCompanyId(), $item->GetDate(), $item->GetStatus(), $item->GetRemark());
    }

    protected static function bindUpdateStatement($stmt, $item, $user) {
        $stmt->bind_param(self::GetBindTypes() . "i", $item->GetId(), $item->GetUserId(), $item->GetCompanyId(), $item->GetDate(), $item->GetStatus(), $item->GetRemark(), $item->GetId());
    }

    //protected static function buildUpdateStatement(){
    //  return "UPDATE reservation SET id = ?, user_id = ?, company_id = ?, date = ?, status = ?, remark = ? WHERE id = ?";
    //}
}
