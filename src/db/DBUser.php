<?php

namespace db;

use model\User as User;
use \shared\QueryOptions as QueryOptions;
use \mysqli as mysqli;

class DBUser extends DBBase {

    const ATTRIBUTES = "id, name, surname, city, address, latitude, longitude, phone, show_number";
    const BIND_TYPES = "issssddss";
    const TABLE_NAME = \config\DB::USER_TABLE;//"user";

    public static function GetAttributes() {
        return self::ATTRIBUTES;
    }

    public static function GetBindTypes() {
        return self::BIND_TYPES;
    }

    public static function GetTableName() {
        return self::TABLE_NAME;
    }

    public static function GetAll(mysqli $db, QueryOptions $qo) {

        $items = array();
        //$query = "SELECT id, name, surname, city, address, latitude, longitude, phone, show_number FROM user";
        $query = "SELECT " . self::GetAttributes() . " FROM ".self::GetTableName();

        if ($stmt = $db->prepare($query)) {
            $stmt->execute();
            $stmt->bind_result($id, $name, $surname, $city, $address, $latitude, $longitude, $phone, $showNumber);

            while ($stmt->fetch()) {

                try {
                    array_push($items, new User($id, $name, $surname, $city, $address, $latitude, $longitude, $phone, $showNumber));
                } catch (Exception $e) {
                    self::processException($e);
                }
            }

            $stmt->close();
        }

        return $items;
    }

    public static function GetById(mysqli $db, QueryOptions $qo) {

        $item = null;
        //$query = "SELECT id, name, surname, city, address, latitude, longitude, phone, show_number FROM user WHERE id = ?";
        $query = "SELECT " . self::GetAttributes() . " FROM ".self::GetTableName()." WHERE id = ?";

        if ($stmt = $db->prepare($query)) {

            $stmt->bind_param("i", $qo->GetId());
            $stmt->execute();
            $stmt->bind_result($id, $name, $surname, $city, $address, $latitude, $longitude, $phone, $showNumber);

            if ($stmt->fetch()) {

                try {

                    $item = new User($id, $name, $surname, $city, $address, $latitude, $longitude, $phone, $showNumber);
                } catch (Exception $e) {
                    self::processException($e);
                }
            }

            $stmt->close();
        }

        return $item;
    }
    
    public static function Authenticate(mysqli $con, $ema, $pass){

        $user = NULL;
        $query = "SELECT ". self::GetAttributes() ." FROM ".self::GetTableName()." WHERE email = ? AND password = ? LIMIT 1";

        if ($stmt = $con->prepare($query)) {
          $stmt->bind_param("ss", $ema, $pass);

          $stmt->execute();
            $stmt->bind_result($id, $name, $surname, $city, $address, $latitude, $longitude, $phone, $showNumber);

            if ($stmt->fetch()) {

                try {

                    $user = new User($id, $name, $surname, $city, $address, $latitude, $longitude, $phone, $showNumber);
        
                } catch (Exception $e) {
                    self::processException($e);
                }
            }

            $stmt->close();
        }

        return $user;
    }
    
    protected static function buildInsertStatement(){
        $c = get_called_class();
        return "INSERT INTO ".$c::GetTableName()." (".$c::GetAttributes().", email, password) VALUES(".$c::generateQuestionMarks().", ?, ?)";
    }
    
    protected static function bindInsertStatement($stmt, $item, $user) {
        $stmt->bind_param(self::GetBindTypes()."ss", /*$item->GetId()*/$id=NULL, $item->GetName(), $item->GetSurname(), $item->GetCity(), $item->GetAddress(), $item->GetLatitude(), $item->GetLongitude(), $item->GetPhone(), $sn=($item->GetShowNumber()?"1":"0"), $item->GetEmail(), $item->GetPassword());
    }

    protected static function bindUpdateStatement($stmt, $item, $user) {
        $stmt->bind_param(self::GetBindTypes() . "i", $item->GetId(), $item->GetName(), $item->GetSurname(), $item->GetCity(), $item->GetAddress(), $item->GetLatitude(), $item->GetLongitude(), $item->GetPhone(), $sn=($item->GetShowNumber()?"1":"0"), $item->GetId());
    }

    //protected static function buildUpdateStatement(){
    //  return "UPDATE user SET id = ?, name = ?, surname = ?, city = ?, address = ?, latitude = ?, longitude = ?, phone = ?, show_number = ? WHERE id = ?";
    //}
}
