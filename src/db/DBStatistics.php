<?php

namespace db;

use \shared\QueryOptions as QueryOptions;
use \mysqli as mysqli;
use \model\Statistics as Statistics;
use \config\DB as DB;

class DBStatistics extends DBBase {
    
    
     public static function GetAnnually(mysqli $db, QueryOptions $qo) {

        $items = array();//ois.quantity
        $query = "SELECT YEAR(o.`date`) `year`, MONTH(o.`date`) `month`, (SELECT SUM(ROUND(m_opt.price * o_it.amount * (1 + o_it.discount/100), 2) + (SELECT COALESCE(SUM(o_it.amount * menu_supp.price), 0)
                                                                                                                                                        FROM ".DB::ORDER_ITEM_SUPPLEMENT_TABLE." ois, ".DB::MENU_ITEM_OPTION_SUPPLEMENT_TABLE." menu_supp 
                                                                                                                                                        WHERE menu_supp.id = ois.menu_item_option_supplement_id
                                                                                                                                                                AND ois.order_item_id = o_it.id)) as sum
                                                                            FROM ".DB::ORDER_ITEM_TABLE." o_it, ".DB::MENU_ITEM_OPTION_TABLE." m_opt
                                                                            WHERE m_opt.id = o_it.menu_item_option_id
                                                                                    AND o.id = o_it.order_id) total
                    FROM ".DB::ORDER_TABLE." o, ".DB::ORDER_ITEM_TABLE." oi
                      WHERE o.id = oi.order_id
                        AND company_id = ".$qo->GetCompanyId()."
                      GROUP BY `month`, `year`
                      ORDER BY `year`,`month` asc";

        if ($stmt = $db->prepare($query)) {
            $stmt->execute();
            $stmt->bind_result($year, $month, $total);
                
            while ($stmt->fetch()) {

                try {
                   
                    array_push($items, new Statistics(array('year'=>$year, 'month'=>$month, 'total'=>$total)));
                    
                } catch (Exception $e) {
                    self::processException($e);
                }                
            }

            $stmt->close();
        }

        return $items;
    }
    
    
    public static function GetMonthlyProducts(mysqli $db, QueryOptions $qo) {

        $items = array();
        /*
        $query = "SELECT m_i.id, m_i.name, m_opt.name, SUM(ROUND(m_opt.price * o_it.amount * (1 + o_it.discount/100), 2) + (SELECT COALESCE(SUM(ois.quantity * menu_supp.price), 0)
                                                                                                                                FROM order_item_supplement ois, menu_item_option_supplement menu_supp 
                                                                                                                                WHERE menu_supp.id = ois.menu_item_option_supplement_id
                                                                                                                                        AND ois.order_item_id = o_it.id)) as sum
	FROM order_item o_it, menu_item_option m_opt, menu_item m_i
        WHERE m_opt.id = o_it.menu_item_option_id
            AND m_i.id = o_it.menu_item_id
            AND o_it.order_id IN (SELECT id FROM `order` o WHERE o.company_id = ".$qo->GetCompanyId()." AND MONTH(o.`date`) = ".$qo->GetMonth().")
        GROUP BY o_it.id";
        */
        
        $query = "SELECT YEAR(o.`date`),
                         m_i.id, 
                         m_i.name, 
                         m_opt.name, 
                         SUM(ROUND(m_opt.price * o_it.amount * (1 + o_it.discount/100), 2) + (SELECT COALESCE(SUM(o_it.amount * menu_supp.price), 0)
                                                                                                FROM ".DB::ORDER_ITEM_SUPPLEMENT_TABLE." ois, ".DB::MENU_ITEM_OPTION_SUPPLEMENT_TABLE." menu_supp 
                                                                                                WHERE menu_supp.id = ois.menu_item_option_supplement_id
                                                                                                        AND ois.order_item_id = o_it.id)) as sum
                                                                                                        
                    FROM ".DB::ORDER_ITEM_TABLE." o_it, ".DB::MENU_ITEM_OPTION_TABLE." m_opt, ".DB::MENU_ITEM_TABLE." m_i, ".DB::ORDER_TABLE." o
                      WHERE m_opt.id = o_it.menu_item_option_id
                          AND m_i.id = o_it.menu_item_id
                          AND o_it.order_id = o.id
                          AND o.company_id = ".$qo->GetCompanyId()."
                          AND MONTH(o.`date`) = ".$qo->GetMonth()."
                      GROUP BY m_opt.id
                      ORDER BY YEAR(o.`date`) asc";

        if ($stmt = $db->prepare($query)) {
            $stmt->execute();
            $stmt->bind_result($year, $item_id, $item_name, $option_name, $total);
                
            while ($stmt->fetch()) {

                try {
                   
                    array_push($items, new Statistics(array('year'=>$year, 'item_id'=>$item_id, 'item_name'=>$item_name, 'option_name'=>$option_name, 'total'=>$total)));
                    
                } catch (Exception $e) {
                    self::processException($e);
                }                
            }

            $stmt->close();
        }

        return $items;
    }
    
    
     public static function GetSummary(mysqli $db, QueryOptions $qo) {

        $item = NULL;
        
        $query = "SELECT
                        (SELECT COUNT(1) FROM ".DB::ORDER_TABLE." WHERE status != 'D' AND company_id = ".$qo->GetCompanyId().") pending,
                        (SELECT COUNT(1) FROM ".DB::ORDER_TABLE." WHERE status = 'D' AND YEAR(`date`) = YEAR(NOW()) AND MONTH(`date`) <= MONTH(NOW()) AND company_id = ".$qo->GetCompanyId().") done,
                        (SELECT COUNT(DISTINCT(user_id)) FROM ".DB::ORDER_TABLE." WHERE status = 'D' AND YEAR(`date`) = YEAR(NOW()) AND MONTH(`date`) <= MONTH(NOW()) AND company_id = ".$qo->GetCompanyId().") users,
                        (SELECT COUNT(1) FROM ".DB::ORDER_TABLE." WHERE status = 'D' AND YEAR(`date`) = YEAR(NOW())-1 AND MONTH(`date`) <= MONTH(NOW()) AND company_id = ".$qo->GetCompanyId().") last_year_orders,
                        (SELECT COUNT(DISTINCT(user_id)) FROM ".DB::ORDER_TABLE." WHERE status = 'D' AND YEAR(`date`) = YEAR(NOW())-1 AND MONTH(`date`) <= MONTH(NOW()) AND company_id = ".$qo->GetCompanyId().") last_year_users
                FROM DUAL";

        if ($stmt = $db->prepare($query)) {
            $stmt->execute();
            $stmt->bind_result($pending, $done, $users, $lastYearOrders, $lastYearUsers);
                
            while ($stmt->fetch()) {

                try {
                   
                    $item = new Statistics(array('pending'=>$pending, 'done'=>$done, 'users'=>$users, 'lastYearOrders'=>$lastYearOrders, 'lastYearUsers'=>$lastYearUsers));
                    
                } catch (Exception $e) {
                    self::processException($e);
                }                
            }

            $stmt->close();
        }

        return $item;
    }
    
    
    
}
