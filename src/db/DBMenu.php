<?php

namespace db;

use model\Menu as Menu;
use \shared\QueryOptions as QueryOptions;
use \mysqli as mysqli;

class DBMenu extends DBBase {

    const ATTRIBUTES = "id, company_id, type, photo";
    const BIND_TYPES = "iiss";
    const TABLE_NAME = \config\DB::MENU_TABLE;//"menu";

    public static function GetAttributes() {
        return self::ATTRIBUTES;
    }

    public static function GetBindTypes() {
        return self::BIND_TYPES;
    }

    public static function GetTableName() {
        return self::TABLE_NAME;
    }

    public static function GetAll(mysqli $db, QueryOptions $qo) {

        $items = array();
        //$query = "SELECT id, company_id, type, photo FROM menu";
        $query = "SELECT " . self::GetAttributes() . " FROM ".self::GetTableName();

        if ($qo->Has(QueryOptions::COMPANY_ID))
            $query .= " WHERE company_id = ".$qo->GetCompanyId();

        if ($stmt = $db->prepare($query)) {

            $stmt->execute();
            $stmt->bind_result($id, $companyId, $type, $photo);

            while ($stmt->fetch()) {

                try {
                    array_push($items, new Menu($id, $companyId, $type, $photo));
                } catch (Exception $e) {
                    self::processException($e);
                }
            }

            $stmt->close();
        }
        
        return $items;
    }

    public static function GetById(mysqli $db, QueryOptions $qo) {

        $item = null;
        //$query = "SELECT id, company_id, type, photo FROM menu WHERE id = ?";
        $query = "SELECT " . self::GetAttributes() . " FROM ".self::GetTableName()." WHERE id = ? AND company_id = ?";

        if ($stmt = $db->prepare($query)) {

            $stmt->bind_param("ii", $qo->GetId(), $qo->GetCompanyId());
            $stmt->execute();
            $stmt->bind_result($id, $companyId, $type, $photo);

            if ($stmt->fetch()) {

                try {

                    $item = new Menu($id, $companyId, $type, $photo);
                } catch (Exception $e) {
                    self::processException($e);
                }
            }

            $stmt->close();
        }

        return $item;
    }

    protected static function bindInsertStatement($stmt, $item, $user) {
        $stmt->bind_param(self::GetBindTypes(), /*$item->GetId()*/$id=NULL, $item->GetCompanyId(), $item->GetType(), $item->GetPhoto());
    }

    protected static function bindUpdateStatement($stmt, $item, $user) {
        $stmt->bind_param(self::GetBindTypes() . "i", $item->GetId(), $item->GetCompanyId(), $item->GetType(), $item->GetPhoto(), $item->GetId());
    }

    //protected static function buildUpdateStatement(){
    //  return "UPDATE menu SET id = ?, company_id = ?, type = ?, photo = ? WHERE id = ?";
    //}
}
