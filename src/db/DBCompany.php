<?php

namespace db;

use model\Company as Company;
use \shared\QueryOptions as QueryOptions;
use \mysqli as mysqli;

class DBCompany extends DBBase {

    const ATTRIBUTES = "id, name, city, address, latitude, longitude, photo, phone, delivery, pick_up, min_delivery_price, max_destination_range, max_free_delivery_range, eth_address, description";
    const BIND_TYPES = "isssddssssiiiss";
    const TABLE_NAME = \config\DB::COMPANY_TABLE;//"company";

    public static function GetAttributes() {
        return self::ATTRIBUTES;
    }

    public static function GetBindTypes() {
        return self::BIND_TYPES;
    }

    public static function GetTableName() {
        return self::TABLE_NAME;
    }

    public static function GetAll(mysqli $db, QueryOptions $qo) {

        $items = array();
        //$query = "SELECT id, name, city, address, latitude, longitude, photo, phone, delivery, pick_up, min_delivery_price, max_destination_range, max_free_delivery_range, description FROM company";
        $query = "SELECT " . self::GetAttributes() . " FROM ".self::GetTableName();
        
        if ($stmt = $db->prepare($query)) {
            $stmt->execute();
            $stmt->bind_result($id, $name, $city, $address, $latitude, $longitude, $photo, $phone, $delivery, $pickUp, $minDeliveryPrice, $maxDestinationRange, $maxFreeDeliveryRange, $ethAddress, $description);

            while ($stmt->fetch()) {

                try {
                    array_push($items, new Company($id, $name, $city, $address, $latitude, $longitude, $photo, $phone, $delivery, $pickUp, $minDeliveryPrice, $maxDestinationRange, $maxFreeDeliveryRange, $ethAddress, $description));
                } catch (Exception $e) {
                    self::processException($e);
                }
            }

            $stmt->close();
        }
        return $items;
    }

    public static function GetById(mysqli $db, QueryOptions $qo) {

        $item = null;
        //$query = "SELECT id, name, city, address, latitude, longitude, photo, phone, delivery, pick_up, min_delivery_price, max_destination_range, max_free_delivery_range, description FROM company WHERE id = ?";
        $query = "SELECT " . self::GetAttributes() . " FROM ".self::GetTableName()." WHERE id = ?";

        if ($stmt = $db->prepare($query)) {

            $stmt->bind_param("i", $qo->GetId());
            $stmt->execute();
            $stmt->bind_result($id, $name, $city, $address, $latitude, $longitude, $photo, $phone, $delivery, $pickUp, $minDeliveryPrice, $maxDestinationRange, $maxFreeDeliveryRange, $ethAddress, $description);

            if ($stmt->fetch()) {

                try {

                    $item = new Company($id, $name, $city, $address, $latitude, $longitude, $photo, $phone, $delivery, $pickUp, $minDeliveryPrice, $maxDestinationRange, $maxFreeDeliveryRange, $ethAddress, $description);
                } catch (Exception $e) {
                    self::processException($e);
                }
            }

            $stmt->close();
        }

        return $item;
    }
    
        public static function Authenticate(mysqli $con, $ema, $pass){

        $item = NULL;
        $query = "SELECT ". self::GetAttributes() ." FROM ".self::GetTableName()." WHERE email = ? AND password = ? LIMIT 1";

        if ($stmt = $con->prepare($query)) {
            $stmt->bind_param("ss", $ema, $pass);

            $stmt->execute();
            $stmt->bind_result($id, $name, $city, $address, $latitude, $longitude, $photo, $phone, $delivery, $pickUp, $minDeliveryPrice, $maxDestinationRange, $maxFreeDeliveryRange, $ethAddress, $description);

            if ($stmt->fetch()) {

                try {

                   $item = new Company($id, $name, $city, $address, $latitude, $longitude, $photo, $phone, $delivery, $pickUp, $minDeliveryPrice, $maxDestinationRange, $maxFreeDeliveryRange, $ethAddress, $description);
                
                } catch (Exception $e) {
                    self::processException($e);
                }
            }

            $stmt->close();
        }

        return $item;
    }
    
     protected static function buildInsertStatement(){
        $c = get_called_class();
        return "INSERT INTO ".$c::GetTableName()." (".$c::GetAttributes().", email, password) VALUES(".$c::generateQuestionMarks().", ?, ?)";
    }

    protected static function bindInsertStatement($stmt, $item, $user) {
        $pickUp = $item->GetPickUp()?"1":"0";
        $delivery = $item->GetDelivery()?"1":"0";
        $stmt->bind_param(self::GetBindTypes()."ss", /*$item->GetId()*/$id=NULL, $item->GetName(), $item->GetCity(), $item->GetAddress(), $item->GetLatitude(), $item->GetLongitude(), $item->GetPhoto(), $item->GetPhone(), $delivery, $pickUp, $item->GetMinDeliveryPrice(), $item->GetMaxDestinationRange(), $item->GetMaxFreeDeliveryRange(), $item->GetEthAddress(), $item->GetDescription(), $item->GetEmail(), $item->GetPassword());
    }

    protected static function bindUpdateStatement($stmt, $item, $user) {
        $pickUp = $item->GetPickUp()?"1":"0";
        $delivery = $item->GetDelivery()?"1":"0";
        $stmt->bind_param(self::GetBindTypes() . "i", $item->GetId(), $item->GetName(), $item->GetCity(), $item->GetAddress(), $item->GetLatitude(), $item->GetLongitude(), $item->GetPhoto(), $item->GetPhone(), $delivery, $pickUp, $item->GetMinDeliveryPrice(), $item->GetMaxDestinationRange(), $item->GetMaxFreeDeliveryRange(), $item->GetEthAddress(), $item->GetDescription(), $item->GetId());
    }

    //protected static function buildUpdateStatement(){
    //  return "UPDATE company SET id = ?, name = ?, city = ?, address = ?, latitude = ?, longitude = ?, photo = ?, phone = ?, delivery = ?, pick_up = ?, min_delivery_price = ?, max_destination_range = ?, max_free_delivery_range = ?, description = ? WHERE id = ?";
    //}
}
