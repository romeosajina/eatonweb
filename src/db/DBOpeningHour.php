<?php

namespace db;

use model\OpeningHour as OpeningHour;
use \shared\QueryOptions as QueryOptions;
use \mysqli as mysqli;

class DBOpeningHour extends DBBase {

    const ATTRIBUTES = "id, company_id, `day`, `from`, `to`";
    const BIND_TYPES = "iiiss";
    const TABLE_NAME = \config\DB::OPENING_HOUR_TABLE;//"opening_hour";

    public static function GetAttributes() {
        return self::ATTRIBUTES;
    }

    public static function GetBindTypes() {
        return self::BIND_TYPES;
    }

    public static function GetTableName() {
        return self::TABLE_NAME;
    }

    public static function GetAll(mysqli $db, QueryOptions $qo) {

        $items = array();
        //$query = "SELECT id, company_id, day, from, to FROM opening_hour";
        $query = "SELECT " . self::GetAttributes() . " FROM ".self::GetTableName()." WHERE company_id = " . $qo->GetCompanyId();

        if ($stmt = $db->prepare($query)) {
            $stmt->execute();
            $stmt->bind_result($id, $companyId, $day, $from, $to);

            while ($stmt->fetch()) {

                try {
                    array_push($items, new OpeningHour($id, $companyId, $day, $from, $to));
                } catch (Exception $e) {
                    self::processException($e);
                }
            }

            $stmt->close();
        }

        return $items;
    }

    public static function GetById(mysqli $db, QueryOptions $qo) {

        $item = null;
        //$query = "SELECT id, company_id, day, from, to FROM opening_hour WHERE id = ?";
        $query = "SELECT " . self::GetAttributes() . " FROM ".self::GetTableName()." WHERE id = ? AND company_id = ?";

        if ($stmt = $db->prepare($query)) {

            $stmt->bind_param("ii", $qo->GetId(), $qo->GetCompanyId());
            $stmt->execute();
            $stmt->bind_result($id, $companyId, $day, $from, $to);

            if ($stmt->fetch()) {

                try {

                    $item = new OpeningHour($id, $companyId, $day, $from, $to);
                } catch (Exception $e) {
                    self::processException($e);
                }
            }

            $stmt->close();
        }

        return $item;
    }

    protected static function bindInsertStatement($stmt, $item, $user) {
        $stmt->bind_param(self::GetBindTypes(), /*$item->GetId()*/$id=NULL, $item->GetCompanyId(), $item->GetDay(), $item->GetFrom(), $item->GetTo());
    }

    protected static function bindUpdateStatement($stmt, $item, $user) {
        $stmt->bind_param(self::GetBindTypes() . "i", $item->GetId(), $item->GetCompanyId(), $item->GetDay(), $item->GetFrom(), $item->GetTo(), $item->GetId());
    }

    //protected static function buildUpdateStatement(){
    //  return "UPDATE opening_hour SET id = ?, company_id = ?, day = ?, from = ?, to = ? WHERE id = ?";
    //}
}
