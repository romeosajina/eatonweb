<?php

namespace db;

use model\MenuItemOption as MenuItemOption;
use \shared\QueryOptions as QueryOptions;
use \mysqli as mysqli;

class DBMenuItemOption extends DBBase {

    const ATTRIBUTES = "id, menu_item_id, name, price";
    const BIND_TYPES = "iisi";
    const TABLE_NAME = \config\DB::MENU_ITEM_OPTION_TABLE;//"menu_item_option";

    public static function GetAttributes() {
        return self::ATTRIBUTES;
    }

    public static function GetBindTypes() {
        return self::BIND_TYPES;
    }

    public static function GetTableName() {
        return self::TABLE_NAME;
    }

    public static function GetAll(mysqli $db, QueryOptions $qo) {

        $items = array();
        //$query = "SELECT id, menu_item_id, name, price FROM menu_item_option";
        $query = "SELECT " . self::GetAttributes() . " FROM ".self::GetTableName();
        
        if($qo->Has(QueryOptions::MENU_ITEM_ID))
            $query .= " WHERE menu_item_id = " . $qo->GetMenuItemId();

        if ($stmt = $db->prepare($query)) {
            $stmt->execute();
            $stmt->bind_result($id, $menuItemId, $name, $price);

            while ($stmt->fetch()) {

                try {
                    array_push($items, new MenuItemOption($id, $menuItemId, $name, $price));
                } catch (Exception $e) {
                    self::processException($e);
                }
            }

            $stmt->close();
        }

        return $items;
    }

    public static function GetById(mysqli $db, QueryOptions $qo) {

        $item = null;
        //$query = "SELECT id, menu_item_id, name, price FROM menu_item_option WHERE id = ?";
        $query = "SELECT " . self::GetAttributes() . " FROM ".self::GetTableName()." WHERE id = ?";

        if ($stmt = $db->prepare($query)) {

            $stmt->bind_param("i", $qo->GetId());
            $stmt->execute();
            $stmt->bind_result($id, $menuItemId, $name, $price);

            if ($stmt->fetch()) {

                try {

                    $item = new MenuItemOption($id, $menuItemId, $name, $price);
                } catch (Exception $e) {
                    self::processException($e);
                }
            }

            $stmt->close();
        }

        return $item;
    }

    protected static function bindInsertStatement($stmt, $item, $user) {
        $stmt->bind_param(self::GetBindTypes(), /*$item->GetId()*/$id=NULL, $item->GetMenuItemId(), $item->GetName(), $item->GetPrice());
    }

    protected static function bindUpdateStatement($stmt, $item, $user) {
        $stmt->bind_param(self::GetBindTypes() . "i", $item->GetId(), $item->GetMenuItemId(), $item->GetName(), $item->GetPrice(), $item->GetId());
    }

    //protected static function buildUpdateStatement(){
    //  return "UPDATE menu_item_option SET id = ?, menu_item_id = ?, name = ?, price = ? WHERE id = ?";
    //}
}
