<?php

namespace db;

use model\OrderItem as OrderItem;
use \shared\QueryOptions as QueryOptions;
use \mysqli as mysqli;
use \config\DB as DB;

class DBOrderItem extends DBBase {

    const ATTRIBUTES = "id, order_id, menu_item_id, menu_item_option_id, amount, discount, remark";
    const BIND_TYPES = "iiiiiis";
    const TABLE_NAME = DB::ORDER_ITEM_TABLE;//"order_item";
  
      public static function GetAttributes() {
        return self::ATTRIBUTES;
    }

    public static function GetBindTypes() {
        return self::BIND_TYPES;
    }

    public static function GetTableName() {
        return self::TABLE_NAME;
    }
    
    const QUERY = "SELECT item.id, item.order_id, item.menu_item_id, item.menu_item_option_id, item.amount, item.discount, item.remark, 
                          m_item.name, m_item.price, m_item.photo, m_item.discount, m_item.description,
                          opt.name, opt.price 
                         FROM ".self::TABLE_NAME." item, 
                              ".DB::MENU_ITEM_OPTION_TABLE." opt,
                              ".DB::MENU_ITEM_TABLE." m_item
                         WHERE opt.id = menu_item_option_id
                             AND item.menu_item_id = m_item.id";

  

    public static function GetAll(mysqli $db, QueryOptions $qo) {

        $items = array();
        //$query = "SELECT id, menu_item_id, menu_item_option_id, amount, discount, remark FROM order_item";
        //$query = "SELECT " . self::GetAttributes() . " FROM order_item";
        //$query = "SELECT item.id, item.menu_item_id, item.menu_item_option_id, item.amount, item.discount, item.remark, opt.name, opt.price 
        //            FROM order_item item, menu_item_option opt WHERE opt.id = menu_item_option_id";
        $query = self::QUERY;
                
        if($qo->Has(QueryOptions::ORDER_ID))
            $query .= " AND order_id = " . $qo->GetOrderId();

        if ($stmt = $db->prepare($query)) {
            $stmt->execute();
            $stmt->bind_result($id, $orderId, $menuItemId, $menuItemOptionId, $amount, $discount, $remark, $menuItemName, $menuItemPrice, $menuItemPhoto, $menuItemDiscount, $menuItemDescription, $menuItemOptionName, $menuItemOptionPrice);

            while ($stmt->fetch()) {

                try {
                    array_push($items, new OrderItem($id, $orderId, $menuItemId, $menuItemOptionId, $amount, $discount, $remark, $menuItemName, $menuItemPrice, $menuItemPhoto, $menuItemDiscount, $menuItemDescription, $menuItemOptionName, $menuItemOptionPrice));
                } catch (Exception $e) {
                    self::processException($e);
                }
            }

            $stmt->close();
        }

        return $items;
    }

    public static function GetById(mysqli $db, QueryOptions $qo) {

        $item = null;
        //$query = "SELECT id, menu_item_id, menu_item_option_id, amount, discount, remark FROM order_item WHERE id = ?";
        //$query = "SELECT " . self::GetAttributes() . " FROM order_item WHERE id = ?";
        //$query = "SELECT item.id, item.menu_item_id, item.menu_item_option_id, item.amount, item.discount, item.remark, opt.name, opt.price 
        //                    FROM order_item item, menu_item_option opt WHERE opt.id = menu_item_option_id AND item.id = ?";
        
        $query = self::QUERY . " AND item.id = ?";

        if ($stmt = $db->prepare($query)) {

            $stmt->bind_param("i", $qo->GetId());
            $stmt->execute();
            $stmt->bind_result($id, $orderId, $menuItemId, $menuItemOptionId, $amount, $discount, $remark, $menuItemName, $menuItemPrice, $menuItemPhoto, $menuItemDiscount, $menuItemDescription, $menuItemOptionName, $menuItemOptionPrice);

            if ($stmt->fetch()) {

                try {

                    $item = new OrderItem($id, $orderId, $menuItemId, $menuItemOptionId, $amount, $discount, $remark, $menuItemName, $menuItemPrice, $menuItemPhoto, $menuItemDiscount, $menuItemDescription, $menuItemOptionName, $menuItemOptionPrice);
                } catch (Exception $e) {
                    self::processException($e);
                }
            }

            $stmt->close();
        }

        return $item;
    }

    protected static function bindInsertStatement($stmt, $item, $user) {
        $stmt->bind_param(self::GetBindTypes(), /*$item->GetId()*/$id=NULL, $item->GetOrderId(), $item->GetMenuItemId(), $item->GetMenuItemOptionId(), $item->GetAmount(), $item->GetDiscount(), $item->GetRemark());
    }

    protected static function bindUpdateStatement($stmt, $item, $user) {
        $stmt->bind_param(self::GetBindTypes() . "i", $item->GetId(), $item->GetOrderId(), $item->GetMenuItemId(), $item->GetMenuItemOptionId(), $item->GetAmount(), $item->GetDiscount(), $item->GetRemark(), $item->GetId());
    }

    //protected static function buildUpdateStatement(){
    //  return "UPDATE order_item SET id = ?, menu_item_id = ?, menu_item_option_id = ?, amount = ?, discount = ?, remark = ? WHERE id = ?";
    //}
}
