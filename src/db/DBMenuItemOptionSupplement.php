<?php

namespace db;

use model\MenuItemOptionSupplement as MenuItemOptionSupplement;
use \shared\QueryOptions as QueryOptions;
use \mysqli as mysqli;

class DBMenuItemOptionSupplement extends DBBase {

    const ATTRIBUTES = "id, menu_item_option_id, name, price, photo";
    const BIND_TYPES = "iisis";
    const TABLE_NAME = \config\DB::MENU_ITEM_OPTION_SUPPLEMENT_TABLE;//"menu_item_option_supplement";

    public static function GetAttributes() {
        return self::ATTRIBUTES;
    }

    public static function GetBindTypes() {
        return self::BIND_TYPES;
    }

    public static function GetTableName() {
        return self::TABLE_NAME;
    }

    public static function GetAll(mysqli $db, QueryOptions $qo) {

        $items = array();
        //$query = "SELECT id, menu_item_option_id, name, price, quantity, photo FROM menu_item_option_supplement";
        $query = "SELECT " . self::GetAttributes() . " FROM ".self::GetTableName();
        
        if($qo->Has(QueryOptions::MENU_ITEM_OPTION_ID))
            $query .= " WHERE menu_item_option_id = " . $qo->GetMenuItemOptionId();

        if ($stmt = $db->prepare($query)) {
            $stmt->execute();
            $stmt->bind_result($id, $menuItemOptionId, $name, $price, $photo);

            while ($stmt->fetch()) {

                try {
                    array_push($items, new MenuItemOptionSupplement($id, $menuItemOptionId, $name, $price, $photo));
                } catch (Exception $e) {
                    self::processException($e);
                }
            }

            $stmt->close();
        }

        return $items;
    }

    public static function GetById(mysqli $db, QueryOptions $qo) {

        $item = null;
        //$query = "SELECT id, menu_item_option_id, name, price, quantity, photo FROM menu_item_option_supplement WHERE id = ?";
        $query = "SELECT " . self::GetAttributes() . " FROM ".self::GetTableName()." WHERE id = ?";

        if ($stmt = $db->prepare($query)) {

            $stmt->bind_param("i", $qo->GetId());
            $stmt->execute();
            $stmt->bind_result($id, $menuItemOptionId, $name, $price, $photo);

            if ($stmt->fetch()) {

                try {

                    $item = new MenuItemOptionSupplement($id, $menuItemOptionId, $name, $price, $photo);
                } catch (Exception $e) {
                    self::processException($e);
                }
            }

            $stmt->close();
        }

        return $item;
    }

    protected static function bindInsertStatement($stmt, $item, $user) {
        $stmt->bind_param(self::GetBindTypes(), /*$item->GetId()*/$id=NULL, $item->GetMenuItemOptionId(), $item->GetName(), $item->GetPrice(), $item->GetPhoto());
    }

    protected static function bindUpdateStatement($stmt, $item, $user) {
        $stmt->bind_param(self::GetBindTypes() . "i", $item->GetId(), $item->GetMenuItemOptionId(), $item->GetName(), $item->GetPrice(), $item->GetPhoto(), $item->GetId());
    }

    //protected static function buildUpdateStatement(){
    //  return "UPDATE menu_item_option_supplement SET id = ?, menu_item_option_id = ?, name = ?, price = ?, quantity = ?, photo = ? WHERE id = ?";
    //}
}
