<?php

namespace db;

use model\Order as Order;
use \shared\QueryOptions as QueryOptions;
use \mysqli as mysqli;

class DBOrder extends DBBase {

    const ATTRIBUTES = "id, user_id, company_id, date, delivery_time, status, rate, remark, payment_method";
    const BIND_TYPES = "iiisssiss";
    const TABLE_NAME = \config\DB::ORDER_TABLE;//"`order`";

    public static function GetAttributes() {
        return self::ATTRIBUTES;
    }

    public static function GetBindTypes() {
        return self::BIND_TYPES;
    }

    public static function GetTableName() {
        return self::TABLE_NAME;
    }

    public static function GetAll(mysqli $db, QueryOptions $qo) {

        $items = array();
        //$query = "SELECT id, user_id, company_id, date, delivery_time, status, rate, remark FROM order";
        $query = "SELECT " . self::GetAttributes() . " FROM " . self::GetTableName();//. " WHERE status != 'D' ";
       
        
        if(gettype($qo->GetOnlyDoneOrders()) == "NULL")
            $query .= " WHERE 1 = 1 ";
        else if($qo->GetOnlyDoneOrders())
            $query .= " WHERE `status` = 'D' ";
        else
            $query .= " WHERE `status` != 'D' ";

        
        if($qo->Has(QueryOptions::COMPANY_ID))
            $query .= "AND company_id = " . $qo->GetCompanyId();
        
        else if($qo->Has(QueryOptions::USER_ID))
            $query .= "AND user_id = " . $qo->GetUserId();
        
        $query .= " ORDER BY id DESC";
        
        if ($stmt = $db->prepare($query)) {
            $stmt->execute();
            $stmt->bind_result($id, $userId, $companyId, $date, $deliveryTime, $status, $rate, $remark, $paymentMethod);

            while ($stmt->fetch()) {

                try {
                    array_push($items, new Order($id, $userId, $companyId, $date, $deliveryTime, $status, $rate, $remark, $paymentMethod));
                } catch (Exception $e) {
                    self::processException($e);
                }
            }

            $stmt->close();
        }

        return $items;
    }

    public static function GetById(mysqli $db, QueryOptions $qo) {

        $item = null;
        //$query = "SELECT id, user_id, company_id, date, delivery_time, status, rate, remark FROM order WHERE id = ?";
        $query = "SELECT " . self::GetAttributes() . " FROM " . self::GetTableName() . " WHERE id = ?";

        if ($stmt = $db->prepare($query)) {

            $stmt->bind_param("i", $qo->GetId());
            $stmt->execute();
            $stmt->bind_result($id, $userId, $companyId, $date, $deliveryTime, $status, $rate, $remark, $paymentMethod);

            if ($stmt->fetch()) {

                try {

                    $item = new Order($id, $userId, $companyId, $date, $deliveryTime, $status, $rate, $remark, $paymentMethod);
                } catch (Exception $e) {
                    self::processException($e);
                }
            }

            $stmt->close();
        }

        return $item;
    }

    protected static function bindInsertStatement($stmt, $item, $user) {
        $stmt->bind_param(self::GetBindTypes(), /*$item->GetId()*/$id=NULL, $item->GetUserId(), $item->GetCompanyId(), $item->GetDate(), $item->GetDeliveryTime(), $item->GetStatus(), $item->GetRate(), $item->GetRemark(), $item->GetPaymentMethod());
    }

    protected static function bindUpdateStatement($stmt, $item, $user) {
        $stmt->bind_param(self::GetBindTypes() . "i", $item->GetId(), $item->GetUserId(), $item->GetCompanyId(), $item->GetDate(), $item->GetDeliveryTime(), $item->GetStatus(), $item->GetRate(), $item->GetRemark(), $item->GetPaymentMethod(), $item->GetId());
    }

    //protected static function buildUpdateStatement(){
    //  return "UPDATE order SET id = ?, user_id = ?, company_id = ?, date = ?, delivery_time = ?, status = ?, rate = ?, remark = ? WHERE id = ?";
    //}
    
    /*
     protected static function buildDeleteStatement(\shared\QueryOptions $qo){
        
        $s = ""; 
         
        if($qo->GetDeleteAllConnectedChildren()){
            
            $s .= "DELETE FROM order_item_supplement WHERE order_item_id IN (SELECT id FROM order_item WHERE order_id = ".$qo->GetId().");";
            
            $s .= "DELETE FROM order_item WHERE order_id = ".$qo->GetId().";";
        }
        
        $s .= "DELETE FROM ".self::GetTableName()." WHERE id = ?";
        
        //var_dump($s);
        
        return $s;
    }
    */
      public static function Delete(mysqli $db, \shared\QueryOptions $qo, $user){
                  
        $stmt = $db->prepare("DELETE FROM ".\db\DBOrderItemSupplement::GetTableName()." WHERE order_item_id IN (SELECT id FROM ".\db\DBOrderItem::GetTableName()." WHERE order_id = ?)");
        $stmt->bind_param("i", $qo->GetId());

        if ($stmt->execute() === false) {
            self::processStmtException($stmt);
            return;
        }
        
        $stmt = $db->prepare("DELETE FROM ".\db\DBOrderItem::GetTableName()." WHERE order_id = ?");
        $stmt->bind_param("i", $qo->GetId());

        if ($stmt->execute() === false) {
            self::processStmtException($stmt);
            return;
        }
        
        $stmt = $db->prepare("DELETE FROM ".self::GetTableName()." WHERE id = ?");
        $stmt->bind_param("i", $qo->GetId());

        if ($stmt->execute() === false) {
            self::processStmtException($stmt);
            return;
        }
        
        $stmt->close();
    }
}
