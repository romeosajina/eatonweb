<?php

namespace model;

class Menu extends BaseModel {

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $companyId;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $photo;

    /**
     * @var MenuItem[]
     */
    private $menuItems;

    public function __construct($id, $companyId, $type, $photo) {
        $this->SetId($id);
        $this->SetCompanyId($companyId);
        $this->SetType($type);
        $this->SetPhoto($photo);
    }

    public function SetId($value) {
        $this->ValidateNumericAttribute('id', $value);
        $this->id = $value;
    }

    public function GetId() {
        return $this->id;
    }

    public function SetCompanyId($value) {
        $this->ValidateNumericAttribute('companyId', $value);
        $this->companyId = $value;
    }

    public function GetCompanyId() {
        return $this->companyId;
    }

    public function SetType($value) {
        $this->ValidateAttribute('type', $value);
        $this->type = $value;
    }

    public function GetType() {
        return $this->type;
    }

    public function SetPhoto($value) {
        $this->photo = $value;
    }

    public function GetPhoto() {
        return $this->photo;
    }

    public function SetMenuItems($value) {
        $this->menuItems = $value;
    }

    public function AddMenuItem($value) {
        array_push($this->menuItems, $value);
    }

    public function GetMenuItems() {
        return $this->menuItems;
    }

    public function PrepareJSON() {
        $prep = array('id' => $this->GetId(),
                      'companyId' => $this->GetCompanyId(),
                      'type' => $this->GetType(),
                      'photo' => $this->GetPhoto());

        $prep = $this->addToJSON('menuItems', $prep, $this->GetMenuItems());

        return $prep;
    }

}
