<?php

namespace model;

class MenuItemOptionSupplement extends BaseModel {

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $menuItemOptionId;

    /**
     * @var string
     */
    private $name;

    /**
     * @var integer
     */
    private $price;

    /**
     * @var string
     */
    private $photo;

    public function __construct($id, $menuItemOptionId, $name, $price, $photo) {
        $this->SetId($id);
        $this->SetMenuItemOptionId($menuItemOptionId);
        $this->SetName($name);
        $this->SetPrice($price);
        $this->SetPhoto($photo);
    }

    public function SetId($value) {
        $this->ValidateNumericAttribute('id', $value);
        $this->id = $value;
    }

    public function GetId() {
        return $this->id;
    }

    public function SetMenuItemOptionId($value) {
        $this->ValidateNumericAttribute('menuItemOptionId', $value);
        $this->menuItemOptionId = $value;
    }

    public function GetMenuItemOptionId() {
        return $this->menuItemOptionId;
    }

    public function SetName($value) {
        $this->ValidateAttribute('name', $value);
        $this->name = $value;
    }

    public function GetName() {
        return $this->name;
    }

    public function SetPrice($value) {
        $this->ValidateDecimalAttribute('price', $value, '(8,2)');
        $this->price = $value;
    }

    public function GetPrice() {
        return $this->price;
    }

    public function SetPhoto($value) {
        $this->photo = $value;
    }

    public function GetPhoto() {
        return $this->photo;
    }

    public function PrepareJSON() {
        $prep = array('id' => $this->GetId(),
            'menuItemOptionId' => $this->GetMenuItemOptionId(),
            'name' => $this->GetName(),
            'price' => $this->GetPrice(),
            'photo' => $this->GetPhoto());
        
        return $prep;
    }

}
