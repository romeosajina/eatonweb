<?php


namespace model;

abstract class AppUser extends BaseModel{
    
    public abstract function isUser();
    public function isCompany(){ return !$this->isUser(); }
    
    public abstract function GetId();

}
