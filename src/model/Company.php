<?php

namespace model;

class Company extends AppUser {

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $city;

    /**
     * @var string
     */
    private $address;

    /**
     * @var integer
     */
    private $latitude;

    /**
     * @var integer
     */
    private $longitude;

    /**
     * @var string
     */
    private $photo;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var unknown
     */
    private $delivery;

    /**
     * @var unknown
     */
    private $pickUp;

    /**
     * @var integer
     */
    private $minDeliveryPrice;

    /**
     * @var integer
     */
    private $maxDestinationRange;

    /**
     * @var integer
     */
    private $maxFreeDeliveryRange;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $ethAddress;

    /**
     * @var OpeningHour[]
     */
    private $openingHours;

    /**
     * @var Menu[]
     */
    private $menus;
    
    /**
     * @var string
     */
    private $email;
    /**
     * @var string
     */
    private $password;


    public function __construct($id, $name, $city, $address, $latitude, $longitude, $photo, $phone, $delivery, $pickUp, $minDeliveryPrice, $maxDestinationRange, $maxFreeDeliveryRange, $ethAddress, $description) {
        $this->SetId($id);
        $this->SetName($name);
        $this->SetCity($city);
        $this->SetAddress($address);
        $this->SetLatitude($latitude);
        $this->SetLongitude($longitude);
        $this->SetPhoto($photo);
        $this->SetPhone($phone);
        $this->SetDelivery($delivery);
        $this->SetPickUp($pickUp);
        $this->SetMinDeliveryPrice($minDeliveryPrice);
        $this->SetMaxDestinationRange($maxDestinationRange);
        $this->SetMaxFreeDeliveryRange($maxFreeDeliveryRange);
        $this->SetEthAddress($ethAddress);
        $this->SetDescription($description);
    }

    public function SetId($value) {
        $this->ValidateNumericAttribute('id', $value);
        $this->id = $value;
    }

    public function GetId() {
        return $this->id;
    }

    public function SetName($value) {
        $this->ValidateAttribute('name', $value);
        $this->name = $value;
    }

    public function GetName() {
        return $this->name;
    }

    public function SetCity($value) {
        $this->ValidateAttribute('city', $value);
        $this->city = $value;
    }

    public function GetCity() {
        return $this->city;
    }

    public function SetAddress($value) {
        $this->ValidateAttribute('address', $value);
        $this->address = $value;
    }

    public function GetAddress() {
        return $this->address;
    }

    public function SetLatitude($value) {
        $this->ValidateDecimalAttribute('latitude', $value, '(10,7)');
        $this->latitude = $value;
    }

    public function GetLatitude() {
        return $this->latitude;
    }

    public function SetLongitude($value) {
        $this->ValidateDecimalAttribute('longitude', $value, '(10,7)');
        $this->longitude = $value;
    }

    public function GetLongitude() {
        return $this->longitude;
    }

    public function SetPhoto($value) {
        //$this->ValidateAttribute('photo', $value);
        $this->photo = $value;
    }

    public function GetPhoto() {
        return $this->photo;
    }

    public function SetPhone($value) {
        $this->ValidateAttribute('phone', $value);
        $this->phone = $value;
    }

    public function GetPhone() {
        return $this->phone;
    }

    public function SetDelivery($value) {
        //$this->ValidateAttribute('delivery', $value);
        $value = $this->ValidateBooleanAttribute('delivery', $value);
        $this->delivery = $value;
    }

    public function GetDelivery() {
        return $this->delivery;
    }

    public function SetPickUp($value) {
        //$this->ValidateAttribute('pickUp', $value);
        $value = $this->ValidateBooleanAttribute('pickUp', $value);
        $this->pickUp = $value;
    }

    public function GetPickUp() {
        return $this->pickUp;
    }

    public function SetMinDeliveryPrice($value) {
        $this->minDeliveryPrice = $value;
    }

    public function GetMinDeliveryPrice() {
        return $this->minDeliveryPrice;
    }

    public function SetMaxDestinationRange($value) {
        $this->maxDestinationRange = $value;
    }

    public function GetMaxDestinationRange() {
        return $this->maxDestinationRange;
    }

    public function SetMaxFreeDeliveryRange($value) {
        $this->maxFreeDeliveryRange = $value;
    }

    public function GetMaxFreeDeliveryRange() {
        return $this->maxFreeDeliveryRange;
    }
    
    public function SetEthAddress($value) {
        $this->ethAddress = $value;
    }

    public function GetEthAddress() {
        return $this->ethAddress;
    }
    
    public function SetDescription($value) {
        $this->description = $value;
    }

    public function GetDescription() {
        return $this->description;
    }

    public function SetOpeningHours($value) {
        $this->openingHours = $value;
    }

    public function AddOpeningHour($value) {
        array_push($this->openingHours, $value);
    }

    public function GetOpeningHours() {
        return $this->openingHours;
    }

    public function SetMenus($value) {
        $this->menus = $value;
    }

    public function AddMenu($value) {
        array_push($this->menus, $value);
    }

    public function GetMenus() {
        return $this->menus;
    }
    
    public function GetEmail(){ return $this->email; }
    public function SetEmail($value){ $this->email = $value; }
    public function GetPassword(){ return $this->password; }
    public function SetPassword($value){ $this->password = $value; }

    public function PrepareJSON() {
        $prep = array('id' => $this->GetId(),
            'name' => $this->GetName(),
            'city' => $this->GetCity(),
            'address' => $this->GetAddress(),
            'latitude' => $this->GetLatitude(),
            'longitude' => $this->GetLongitude(),
            'photo' => $this->GetPhoto(),
            'phone' => $this->GetPhone(),
            'delivery' => $this->GetDelivery(),
            'pickUp' => $this->GetPickUp(),
            'minDeliveryPrice' => $this->GetMinDeliveryPrice(),
            'maxDestinationRange' => $this->GetMaxDestinationRange(),
            'maxFreeDeliveryRange' => $this->GetMaxFreeDeliveryRange(),
            'ethAddress' => $this->GetEthAddress(),
            'description' => $this->GetDescription());

        $prep = $this->addToJSON('openingHours', $prep, $this->GetOpeningHours());
        $prep = $this->addToJSON('menus', $prep, $this->GetMenus());

        return $prep;
    }

    public function isUser() { return FALSE; }

}
