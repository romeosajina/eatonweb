<?php

namespace model;

class BaseModel {
    
    protected function ValidateAttribute($attrName, $value){
        if($value == "" || $value == null){
            throw new \InvalidArgumentException("Attribute '$attrName' is mandatory (provided value is: $value)");
        }
    }

    protected function ValidateNumericAttribute($attrName, $value){
        
        if(!is_numeric($value)){// || $value == "" || $value == null){
            throw new \InvalidArgumentException("Attribute '$attrName' must be in numeric form (provided value is: $value)");
        }
    }
    protected function ValidateBooleanAttribute($attrName, $value){ 
        
        if($value != "1" && $value != "0" && $value != FALSE && $value != TRUE)
            $this->ValidateAttribute($attrName, $value); 
        
        return $value == "1" || $value == "0" ? $value == "1" : $value;    
    }
    
    protected function ValidateDateAttribute($attrName, $value){ $this->ValidateAttribute($attrName, $value); }
    
    protected function ValidateEmailAttribute($value, $attrName){
        if($value == "" || $value == null || filter_var($value, FILTER_VALIDATE_EMAIL) === false){
            throw new \InvalidArgumentException("Attribute '$attrName' must be valid email (provided value is: $value)");
        }
    }
    
    protected function ValidateDecimalAttribute($attrName, $value, $decimalExpression/*npr. (10,2)*/){
        $this->ValidateNumericAttribute($attrName, $value);
    }
    
    protected function ValidateValueInAttribute($attrName, $value, $values){
        if(array_key_exists($value, $values) == FALSE){
           
            $vls = "";
            foreach($values as $key => $v){
                
                if($vls != "")
                    $vls .= ", ";
                
                $vls .= $key. " = " . $v;
            }
                    
            throw new \InvalidArgumentException("Attribute '$attrName' must be in values($vls) => provided value is: $value");
        }
    }
    
    public function PrepareJSON(){ return array();}

    public function ToJSON(){
        return json_encode($this->PrepareJSON(), JSON_PRETTY_PRINT/*JSON_UNESCAPED_UNICODE*/);
    }

    public static function AllToJSON($items){
        return json_encode(array(
            'items' => array_map(function ($item) { return $item->PrepareJSON();}, $items)
        ), JSON_PRETTY_PRINT/*JSON_UNESCAPED_UNICODE*/);
      }
      
    public static function AllToArray($items){
        return array_map(function ($item) { return $item->PrepareJSON();}, $items);
      }
     
      
    protected function addToJSON($key, array $prepareJSON, array $arrayItems = null){
       
        if($arrayItems == null && is_array($arrayItems)){
            $prepareJSON[$key] = [];
            
        }else if($arrayItems != null){
            
            $prepareJSON[$key] = array_map(function ($item) { return $item->PrepareJSON();}, $arrayItems);
            
            //array_push($prepareJSON, 'courses' => array_map(function ($item) { return $item->PrepareJSON();}, $arrayItems));
       
        }
       
        return $prepareJSON;
    }

}
/*
public static function checkValue($value, $attrName){
        if($value == "" || $value == null){
            throw new \InvalidArgumentException("Attribute '$attrName' is mandatory (provided value is: $value)");
        }
    }
    
    
    public static function checkMatchedValue($value, $attrName, array $vals){
      
        if($value == "" || $value == null || !in_array($value, $vals)){
            
            throw new \InvalidArgumentException("Attribute '$attrName' must match values (".implode(", ", $vals).") (provided value is: $value)");
            
        }
    }
  */