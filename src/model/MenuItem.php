<?php

namespace model;

class MenuItem extends BaseModel {

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $menuId;

    /**
     * @var string
     */
    private $name;

    /**
     * @var integer
     */
    private $price;

    /**
     * @var integer
     */
    private $rate;

    /**
     * @var string
     */
    private $photo;

    /**
     * @var integer
     */
    private $discount;

    /**
     * @var string
     */
    private $description;

    /**
     * @var MenuItemOption[]
     */
    private $menuItemOptions;
    
    public function __construct($id, $menuId, $name, $price, $rate, $photo, $discount, $description) {
        $this->SetId($id);
        $this->SetMenuId($menuId);
        $this->SetName($name);
        $this->SetPrice($price);
        $this->SetRate($rate);
        $this->SetPhoto($photo);
        $this->SetDiscount($discount);
        $this->SetDescription($description);
    }

    public function SetId($value) {
        $this->ValidateNumericAttribute('id', $value);
        $this->id = $value;
    }

    public function GetId() {
        return $this->id;
    }

    public function SetMenuId($value) {
        $this->ValidateNumericAttribute('menuId', $value);
        $this->menuId = $value;
    }

    public function GetMenuId() {
        return $this->menuId;
    }

    public function SetName($value) {
        $this->ValidateAttribute('name', $value);
        $this->name = $value;
    }

    public function GetName() {
        return $this->name;
    }

    public function SetPrice($value) {
        $this->ValidateDecimalAttribute('price', $value, '(8,2)');
        $this->price = $value;
    }

    public function GetPrice() {
        return $this->price;
    }

    public function SetRate($value) {
        $this->ValidateDecimalAttribute('rate', $value, '(5,2)');
        $this->rate = $value;
    }

    public function GetRate() {
        return $this->rate;
    }

    public function SetPhoto($value) {
        $this->photo = $value;
    }

    public function GetPhoto() {
        return $this->photo;
    }

    public function SetDiscount($value) {
        $this->discount = $value;
    }

    public function GetDiscount() {
        return $this->discount;
    }

    public function SetDescription($value) {
        $this->description = $value;
    }

    public function GetDescription() {
        return $this->description;
    }
    
    public function SetMenuItemOptions($value){
        $this->menuItemOptions = $value;
    }
   
    public function GetMenuItemOptions(){
        return $this->menuItemOptions;
    }

    public function PrepareJSON() {
        $prep = array('id' => $this->GetId(),
            'menuId' => $this->GetMenuId(),
            'name' => $this->GetName(),
            'price' => $this->GetPrice(),
            'rate' => $this->GetRate(),
            'photo' => $this->GetPhoto(),
            'discount' => $this->GetDiscount(),
            'description' => $this->GetDescription());

        $prep = $this->addToJSON('menuItemOptions', $prep, $this->GetMenuItemOptions());

        return $prep;
    }

}
