<?php

namespace model;

class MenuItemOption extends BaseModel {

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $menuItemId;

    /**
     * @var string
     */
    private $name;

    /**
     * @var integer
     */
    private $price;

    /**
     * @var MenuItemOptionSupplement[]
     */
    private $menuItemOptionSupplement;
    
    public function __construct($id, $menuItemId, $name, $price) {
        $this->SetId($id);
        $this->SetMenuItemId($menuItemId);
        $this->SetName($name);
        $this->SetPrice($price);
    }

    public function SetId($value) {
        $this->ValidateNumericAttribute('id', $value);
        $this->id = $value;
    }

    public function GetId() {
        return $this->id;
    }

    public function SetMenuItemId($value) {
        $this->ValidateNumericAttribute('menuItemId', $value);
        $this->menuItemId = $value;
    }

    public function GetMenuItemId() {
        return $this->menuItemId;
    }

    public function SetName($value) {
        $this->ValidateAttribute('name', $value);
        $this->name = $value;
    }

    public function GetName() {
        return $this->name;
    }

    public function SetPrice($value) {
        $this->ValidateNumericAttribute('price', $value);
        $this->price = $value;
    }

    public function GetPrice() {
        return $this->price;
    }
    
    public function SetMenuItemOptionSupplement($value){
        $this->menuItemOptionSupplement = $value;
    }
   
    public function GetMenuItemOptionSupplement(){
        return $this->menuItemOptionSupplement;
    }
    
    public function PrepareJSON() {
        $prep = array('id' => $this->GetId(),
                      'menuItemId' => $this->GetMenuItemId(),
                      'name' => $this->GetName(),
                      'price' => $this->GetPrice());
        
        $prep = $this->addToJSON('menuItemOptionSupplements', $prep, $this->GetMenuItemOptionSupplement());

        return $prep;
    }

}
