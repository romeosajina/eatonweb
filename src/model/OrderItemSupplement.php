<?php

namespace model;

class OrderItemSupplement extends BaseModel {

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $orderItemId;

    /**
     * @var integer
     */
    private $menuItemSupplementId;

    /**
     * @var string
     */
    private $menuItemSupplementName;

    /**
     * @var integer
     */
    private $menuItemSupplementPrice;

    public function __construct($id, $orderItemId, $menuItemSupplementId, $menuItemSupplementName, $menuItemSupplementPrice) {
        $this->SetId($id);
        $this->SetOrderItemId($orderItemId);
        $this->SetMenuItemSupplementId($menuItemSupplementId);
        $this->SetMenuItemSupplementName($menuItemSupplementName);
        $this->SetMenuItemSupplementPrice($menuItemSupplementPrice);
    }

    public function SetId($value) {
        $this->ValidateNumericAttribute('id', $value);
        $this->id = $value;
    }

    public function GetId() {
        return $this->id;
    }

    public function SetOrderItemId($value) {
        $this->ValidateNumericAttribute('orderItemId', $value);
        $this->orderItemId = $value;
    }

    public function GetOrderItemId() {
        return $this->orderItemId;
    }

    public function SetMenuItemSupplementId($value) {
        $this->ValidateNumericAttribute('menuItemOptionSupplementId', $value);
        $this->menuItemSupplementId = $value;
    }

    public function GetMenuItemSupplementId() {
        return $this->menuItemSupplementId;
    }
    
    
    public function SetMenuItemSupplementName($value) { $this->menuItemSupplementName = $value; }
    public function GetMenuItemSupplementName() { return $this->menuItemSupplementName; }
    public function SetMenuItemSupplementPrice($value) { $this->menuItemSupplementPrice = $value; }
    public function GetMenuItemSupplementPrice() { return $this->menuItemSupplementPrice; }

    public function PrepareJSON() {
        $prep = array('id' => $this->GetId(),
                      'orderItemId' => $this->GetOrderItemId(),
          
                      //'menuItemSupplementId' => $this->GetMenuItemSupplementId(),
                      //Ovo je Robert inszitira da se mora zvat tako jer ne znan zas
                      'menuItemOptionSupplementId' => $this->GetMenuItemSupplementId(),
                      'menuItemOptionSupplementName' => $this->GetMenuItemSupplementName(),
                      'menuItemOptionSupplementPrice' => $this->GetMenuItemSupplementPrice());

        return $prep;
    }

}
