<?php

namespace model;

class Reservation extends BaseModel {

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $userId;

    /**
     * @var integer
     */
    private $companyId;

    /**
     * @var timestamp
     */
    private $date;

    /**
     * @var string
     */
    private $status;

    /**
     * @var string
     */
    private $remark;

    public function __construct($id, $userId, $companyId, $date, $status, $remark) {
        $this->SetId($id);
        $this->SetUserId($userId);
        $this->SetCompanyId($companyId);
        $this->SetDate($date);
        $this->SetStatus($status);
        $this->SetRemark($remark);
    }

    public function SetId($value) {
        $this->ValidateNumericAttribute('id', $value);
        $this->id = $value;
    }

    public function GetId() {
        return $this->id;
    }

    public function SetUserId($value) {
        $this->ValidateNumericAttribute('userId', $value);
        $this->userId = $value;
    }

    public function GetUserId() {
        return $this->userId;
    }

    public function SetCompanyId($value) {
        $this->ValidateNumericAttribute('companyId', $value);
        $this->companyId = $value;
    }

    public function GetCompanyId() {
        return $this->companyId;
    }

    public function SetDate($value) {
        $this->ValidateDateAttribute('date', $value);
        $this->date = $value;
    }

    public function GetDate() {
        return $this->date;
    }

    public function SetStatus($value) {
        $this->ValidateAttribute('status', $value);
        $this->status = $value;
    }

    public function GetStatus() {
        return $this->status;
    }

    public function SetRemark($value) {
        $this->remark = $value;
    }

    public function GetRemark() {
        return $this->remark;
    }

    public function PrepareJSON() {
        $prep = array('id' => $this->GetId(),
            'userId' => $this->GetUserId(),
            'companyId' => $this->GetCompanyId(),
            'date' => $this->GetDate(),
            'status' => $this->GetStatus(),
            'remark' => $this->GetRemark());


        return $prep;
    }

}
