<?php

namespace model;

class User extends AppUser{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $surname;

    /**
     * @var string
     */
    private $city;

    /**
     * @var string
     */
    private $address;

    /**
     * @var integer
     */
    private $latitude;

    /**
     * @var integer
     */
    private $longitude;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var boolean
     */
    private $showNumber;
    
        
    /**
     * @var string
     */
    private $email;
    /**
     * @var string
     */
    private $password;


    public function __construct($id, $name, $surname, $city, $address, $latitude, $longitude, $phone, $showNumber) {
        $this->SetId($id);
        $this->SetName($name);
        $this->SetSurname($surname);
        $this->SetCity($city);
        $this->SetAddress($address);
        $this->SetLatitude($latitude);
        $this->SetLongitude($longitude);
        $this->SetPhone($phone);
        $this->SetShowNumber($showNumber);
    }

    public function SetId($value) {
        $this->ValidateNumericAttribute('id', $value);
        $this->id = $value;
    }

    public function GetId() {
        return $this->id;
    }

    public function SetName($value) {
        $this->ValidateAttribute('name', $value);
        $this->name = $value;
    }

    public function GetName() {
        return $this->name;
    }

    public function SetSurname($value) {
        $this->ValidateAttribute('surname', $value);
        $this->surname = $value;
    }

    public function GetSurname() {
        return $this->surname;
    }

    public function SetCity($value) {
        $this->ValidateAttribute('city', $value);
        $this->city = $value;
    }

    public function GetCity() {
        return $this->city;
    }

    public function SetAddress($value) {
        $this->ValidateAttribute('address', $value);
        $this->address = $value;
    }

    public function GetAddress() {
        return $this->address;
    }

    public function SetLatitude($value) {
        $this->ValidateDecimalAttribute('latitude', $value, '(10,7)');
        $this->latitude = $value;
    }

    public function GetLatitude() {
        return $this->latitude;
    }

    public function SetLongitude($value) {
        $this->ValidateDecimalAttribute('longitude', $value, '(10,7)');
        $this->longitude = $value;
    }

    public function GetLongitude() {
        return $this->longitude;
    }

    public function SetPhone($value) {
        $this->phone = $value;
    }

    public function GetPhone() {
        return $this->phone;
    }

    public function SetShowNumber($value) {
        $value = $this->ValidateBooleanAttribute('showNumber', $value);
        $this->showNumber = $value;
    }

    public function GetShowNumber() {
        return $this->showNumber;
    }
    
    public function GetEmail(){ return $this->email; }
    public function SetEmail($value){ $this->email = $value; }
    public function GetPassword(){ return $this->password; }
    public function SetPassword($value){ $this->password = $value; }

    public function PrepareJSON() {
        $prep = array('id' => $this->GetId(),
                      'name' => $this->GetName(),
                      'surname' => $this->GetSurname(),
                      'city' => $this->GetCity(),
                      'address' => $this->GetAddress(),
                      'latitude' => $this->GetLatitude(),
                      'longitude' => $this->GetLongitude(),
                      'phone' => $this->GetShowNumber() == "1"? $this->GetPhone() : "",
                      'showNumber' => $this->GetShowNumber());

        return $prep;
    }
    
    public function isUser() { return TRUE; }

}
