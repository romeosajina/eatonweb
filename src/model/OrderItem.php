<?php

namespace model;

class OrderItem extends BaseModel {

    /**
     * @var integer
     */
    private $id;
    
    /**
     * @var integer
     */
    private $orderId;

    /**
     * @var integer
     */
    private $menuItemId;

    /**
     * @var integer
     */
    private $menuItemOptionId;

    /**
     * @var integer
     */
    private $amount;

    /**
     * @var integer
     */
    private $discount;

    /**
     * @var string
     */
    private $remark;
     
    /**
     * @var string
     */
    private $menuItemName;
    
    /**
     * @var string
     */
    private $menuItemPrice;
    /**
     * @var string
     */
    private $menuItemPhoto;
    /**
     * @var string
     */
    private $menuItemDiscount;
    /**
     * @var string
     */
    private $menuItemDescription; 
    /**
     * @var string
     */
    private $menuItemOptionName;

    /**
     * @var integer
     */
    private $menuItemOptionPrice;

    /**
     * @var OrderItemSupplement[]
     */
    private $orderItemSupplements;

    public function __construct($id, $orderId, $menuItemId, $menuItemOptionId, $amount, $discount, $remark, $menuItemName, $menuItemPrice, $menuItemPhoto, $menuItemDiscount, $menuItemDescription, $menuItemOptionName, $menuItemOptionPrice) {
        $this->SetId($id);
        $this->SetOrderId($orderId);
        $this->SetMenuItemId($menuItemId);
        $this->SetMenuItemOptionId($menuItemOptionId);
        $this->SetAmount($amount);
        $this->SetDiscount($discount);
        $this->SetRemark($remark);      
        
        $this->SetMenuItemName($menuItemName);
        $this->SetMenuItemPrice($menuItemPrice);
        $this->SetMenuItemPhoto($menuItemPhoto);
        $this->SetMenuItemDiscount($menuItemDiscount);
        $this->SetMenuItemDescription($menuItemDescription);
        
        $this->SetMenuItemOptionName($menuItemOptionName);
        $this->SetMenuItemOptionPrice($menuItemOptionPrice);
    }

    public function SetId($value) {
        $this->ValidateNumericAttribute('id', $value);
        $this->id = $value;
    }

    public function GetId() {
        return $this->id;
    }
    
     public function SetOrderId($value) {
        $this->ValidateNumericAttribute('orderId', $value);
        $this->orderId = $value;
    }

    public function GetOrderId() {
        return $this->orderId;
    }


    public function SetMenuItemId($value) {
        $this->ValidateNumericAttribute('menuItemId', $value);
        $this->menuItemId = $value;
    }

    public function GetMenuItemId() {
        return $this->menuItemId;
    }

    public function SetMenuItemOptionId($value) {
        $this->ValidateNumericAttribute('menuItemOptionId', $value);
        $this->menuItemOptionId = $value;
    }

    public function GetMenuItemOptionId() {
        return $this->menuItemOptionId;
    }

    public function SetAmount($value) {
        $this->ValidateNumericAttribute('amount', $value);
        $this->amount = $value;
    }

    public function GetAmount() {
        return $this->amount;
    }

    public function SetDiscount($value) {
        $this->discount = $value;
    }

    public function GetDiscount() {
        return $this->discount;
    }

    public function SetRemark($value) {
        $this->remark = $value;
    }

    public function GetRemark() {
        return $this->remark;
    }

    public function SetOrderItemSupplement($value) {
        $this->orderItemSupplements = $value;
    }

    public function AddOrderItemSupplement($value) {
        array_push($this->orderItemSupplements, $value);
    }

    public function GetOrderItemSupplements() {
        return $this->orderItemSupplements;
    }
    
    public function SetMenuItemName($value){$this->menuItemName = $value; }
    public function SetMenuItemPrice($value){$this->menuItemPrice = $value; }
    public function SetMenuItemPhoto($value){$this->menuItemPhoto = $value; }
    public function SetMenuItemDiscount($value){$this->menuItemDiscount = $value; }
    public function SetMenuItemDescription($value){$this->menuItemDescription = $value; }
    public function SetMenuItemOptionName($value) { $this->menuItemOptionName = $value; }
    public function GetMenuItemName(){ return $this->menuItemName; }
    public function GetMenuItemPrice(){ return $this->menuItemPrice; }
    public function GetMenuItemPhoto(){ return $this->menuItemPhoto; }
    public function GetMenuItemDiscount(){ return $this->menuItemDiscount; }
    public function GetMenuItemDescription(){ return $this->menuItemDescription; }
    public function GetMenuItemOptionName() { return $this->menuItemOptionName; }
    public function SetMenuItemOptionPrice($value) { $this->menuItemOptionPrice = $value; }
    public function GetMenuItemOptionPrice() { return $this->menuItemOptionPrice; }
    
    
    public function PrepareJSON() {
        $prep = array('id' => $this->GetId(),
                      'orderId' => $this->GetOrderId(),
                      'menuItemId' => $this->GetMenuItemId(),
                      'menuItemName' => $this->GetMenuItemName(), 
                      'menuItemPrice' => $this->GetMenuItemPrice(), 
                      'menuItemPhoto' => $this->GetMenuItemPhoto(),
                      'menuItemDiscount' => $this->GetMenuItemDiscount(), 
                      'menuItemDescription' => $this->GetMenuItemDescription(), 
                      'menuItemOptionId' => $this->GetMenuItemOptionId(),
                      'menuItemOptionName' => $this->GetMenuItemOptionName(),
                      'menuItemOptionPrice' => $this->GetMenuItemOptionPrice(),
                      'amount' => $this->GetAmount(),
                      'discount' => $this->GetDiscount(),
                      'remark' => $this->GetRemark());
        

        $prep = $this->addToJSON('orderItemSupplements', $prep, $this->GetOrderItemSupplements());

        return $prep;
    }

}
