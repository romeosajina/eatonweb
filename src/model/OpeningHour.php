<?php

namespace model;

class OpeningHour extends BaseModel {

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $companyId;

    /**
     * @var integer
     */
    private $day;

    /**
     * @var timestamp
     */
    private $from;

    /**
     * @var timestamp
     */
    private $to;

    public function __construct($id, $companyId, $day, $from, $to) {
        $this->SetId($id);
        $this->SetCompanyId($companyId);
        $this->SetDay($day);
        $this->SetFrom($from);
        $this->SetTo($to);
    }

    public function SetId($value) {
        $this->ValidateNumericAttribute('id', $value);
        $this->id = $value;
    }

    public function GetId() {
        return $this->id;
    }

    public function SetCompanyId($value) {
        $this->ValidateNumericAttribute('companyId', $value);
        $this->companyId = $value;
    }

    public function GetCompanyId() {
        return $this->companyId;
    }

    public function SetDay($value) {
        $this->ValidateNumericAttribute('day', $value);
        $this->day = $value;
    }

    public function GetDay() {
        return $this->day;
    }

    public function SetFrom($value) {
        $this->ValidateDateAttribute('from', $value);
        $this->from = $value;
    }

    public function GetFrom() {
        return $this->from;
    }

    public function SetTo($value) {
        $this->ValidateDateAttribute('to', $value);
        $this->to = $value;
    }

    public function GetTo() {
        return $this->to;
    }

    public function PrepareJSON() {
        $prep = array('id' => $this->GetId(),
            'companyId' => $this->GetCompanyId(),
            'day' => $this->GetDay(),
            'from' => $this->GetFrom(),
            'to' => $this->GetTo());


        return $prep;
    }

}
