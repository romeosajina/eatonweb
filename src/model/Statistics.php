<?php

namespace model;


class Statistics extends BaseModel{

    /**
     * @var string
     */
    //private $label;

    /**
     * @var array[]
     */
    private $data;
    
    //public function __construct(...$args) {
    public function __construct($args) {
       $this->data = $args;
    }
    
    public function PrepareJSON() {
        /*$prep = array('label' => $this->label,
                      'data' => $this->data);
        
        return $prep;
         */
        return $this->data;
    }
}
