<?php

namespace model;

class Order extends BaseModel {

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $userId;

    /**
     * @var integer
     */
    private $companyId;

    /**
     * @var timestamp
     */
    private $date;

    /**
     * @var timestamp
     */
    private $deliveryTime;

    /**
     * @var string
     */
    private $status;

    /**
     * @var integer
     */
    private $rate;

    /**
     * @var string
     */
    private $remark;

    /**
     * @var string
     */
    private $paymentMethod;

    /**
     * @var OrderItem[]
     */
    private $orderItems;

    public function __construct($id, $userId, $companyId, $date, $deliveryTime, $status, $rate, $remark, $paymentMethod) {
        $this->SetId($id);
        $this->SetUserId($userId);
        $this->SetCompanyId($companyId);
        $this->SetDate($date);
        $this->SetDeliveryTime($deliveryTime);
        $this->SetStatus($status);
        $this->SetRate($rate);
        $this->SetRemark($remark);
        $this->SetPaymentMethod($paymentMethod);
    }

    public function SetId($value) {
        $this->ValidateNumericAttribute('id', $value);
        $this->id = $value;
    }

    public function GetId() {
        return $this->id;
    }

    public function SetUserId($value) {
        $this->ValidateNumericAttribute('userId', $value);
        $this->userId = $value;
    }

    public function GetUserId() {
        return $this->userId;
    }

    public function SetCompanyId($value) {
        $this->ValidateNumericAttribute('companyId', $value);
        $this->companyId = $value;
    }

    public function GetCompanyId() {
        return $this->companyId;
    }

    public function SetDate($value) {
        $this->ValidateDateAttribute('date', $value);
        $this->date = $value;
    }

    public function GetDate() {
        return $this->date;
    }

    public function SetDeliveryTime($value) {
        $this->ValidateDateAttribute('deliveryTime', $value);
        $this->deliveryTime = $value;
    }

    public function GetDeliveryTime() {
        return $this->deliveryTime;
    }

    public function SetStatus($value) {
        
        // INITIALIZED => Narudzba/rezervacija je tek kreirana - samo u mob/web aplikaciji
        // ORDERED     => Korisnik je potvrdia narudzbu, sad ceka da mu se jave iz company - commit na bazu/notifikacija company
        // CONFIRMED   => Company je potvrdila narudzbu
        // PROCESSING  => Narudzba se sada priprema
        // FINISHED    => Narudzba je obradena i spremna za dostavu - obavjesti korisnika da stize
        // DELIVERED   => Narudzba je dostavljena na adresu

        $this->ValidateValueInAttribute('status', $value, array('O' => 'ordered', 'C' => 'confirmed', 'P' => 'processing', 'F' => 'finished', 'D' => 'delivered'));
        $this->status = $value;
    }

    public function GetStatus() {
        return $this->status;
    }

    public function SetRate($value) {
        $this->ValidateNumericAttribute('rate', $value);
        $this->rate = $value;
    }

    public function GetRate() {
        return $this->rate;
    }

    public function SetRemark($value) {
        $this->remark = $value;
    }

    public function GetRemark() {
        return $this->remark;
    }

    public function SetPaymentMethod($value) {
        
        // ETHERIUM => Plaćeno preko etherium-a
        // CASH     => Plaća se gotovinom prilikom preuzimanja

        $this->ValidateValueInAttribute('paymentMethod', $value, array('E' => 'Etherium', 'C' => 'Cash'));
        $this->paymentMethod = $value;
    }

    public function GetPaymentMethod() {
        return $this->paymentMethod;
    }
    
    public function SetOrderItems($value) {
        $this->orderItems = $value;
    }

    public function AddOrderItem($value) {
        array_push($this->orderItems, $value);
    }

    public function GetOrderItems() {
        return $this->orderItems;
    }

    public function PrepareJSON() {
        $prep = array('id' => $this->GetId(),
            'userId' => $this->GetUserId(),
            'companyId' => $this->GetCompanyId(),
            'date' => $this->GetDate(),
            'deliveryTime' => $this->GetDeliveryTime(),
            'status' => $this->GetStatus(),
            'rate' => $this->GetRate(),
            'remark' => $this->GetRemark(),
            'paymentMethod' => $this->GetPaymentMethod());

        $prep = $this->addToJSON('orderItems', $prep, $this->GetOrderItems());

        return $prep;
    }

}
