<?php

namespace shared;

class QueryOptions {

    /**
     * @var string
     */
    const ID = "id";

    /**
     * @var string
     */
    const COMPANY_ID = "companyId";

    /**
     * @var string
     */
    const MENU_ID = "menuId";

    /**
     * @var string
     */
    const MENU_ITEM_ID = "menuItemId";

    /**
     * @var string
     */
    const MENU_ITEM_OPTION_ID = "menuItemOptionId";

    /**
     * @var string
     */
    const USER_ID = "userId";

    /**
     * @var string
     */
    const ORDER_ITEM_ID = "orderItemId";

    /**
     * @var string
     */
    const MENU_ITEM_SUPPLEMENT_ID = "menuItemSupplementId";

    /**
     * @var string
     */
    const ORDER_ID = "orderId";
    
    /**
     * @var string
     */
    const MONTH = "month";
    
    /**
     * @var string[]
     */
    const ALL_ATTRIBUTES = array(self::ID, self::COMPANY_ID, self::MENU_ID, self::MENU_ITEM_ID, self::MENU_ITEM_OPTION_ID, self::USER_ID, self::ORDER_ITEM_ID, self::MENU_ITEM_SUPPLEMENT_ID, self::ORDER_ID, self::MONTH);
    
    /**
     * @var string
     */
    protected $limit;

    /**
     * @var string
     */
    protected $offset;
    
    /**
     * @var boolean
     */
    protected $deleteAllConnectedChildren = NULL; 
    
    /**
     * @var boolean
     */
    protected $onlyDoneOrders = NULL; 
    
    /**
     * @var array()
     */
    protected $map = array();
    

    public function __construct() {
        
    }

    public function Set($key, $value) {
        $this->map[$key] = $value;
    }

    public function Get($key) {
        return $this->map[$key];
    }
    
    /**
     * @var boolean
     */
    public function Has($key) {
        $val = $this->Get($key);
        
        if(is_numeric($val))
            return TRUE;
        else
            return $val != null && $val != "";
    }
    
    public function SetOffset($value) {
        $this->offset = $value;
    }

    public function SetLimit($value) {
        $this->limit = $value;
    }
    
    public function SetDeleteAllConnectedChildren($value) {
        $this->deleteAllConnectedChildren = $value;
    }
    
    public function SetOnlyDoneOrders($value) {
        $this->onlyDoneOrders = $value;
    }
    
    
    public function GetOffset() {
        return $this->offset;
    }

    public function GetLimit() {
        return $this->limit;
    }

    /**
     * @var boolean
     */
    public function GetDeleteAllConnectedChildren() {
        return $this->deleteAllConnectedChildren;
    }
    
    /**
     * @var boolean
     */
    public function GetOnlyDoneOrders() {
        return $this->onlyDoneOrders;
    }
        
    public function SetId($value) {
        $this->Set(self::ID, $value);
    }

    /**
     * @var integer
     */
    public function GetId() {
        return $this->Get(self::ID);
    }

    public function SetCompanyId($value) {
        $this->Set(self::COMPANY_ID, $value);
    }

    /**
     * @var integer
     */
    public function GetCompanyId() {
        return $this->Get(self::COMPANY_ID);
    }

    public function SetMenuId($value) {
        $this->Set(self::MENU_ID, $value);
    }

    /**
     * @var integer
     */
    public function GetMenuId() {
        return $this->Get(self::MENU_ID);
    }

    public function SetMenuItemId($value) {
        $this->Set(self::MENU_ITEM_ID, $value);
    }

    /**
     * @var integer
     */
    public function GetMenuItemId() {
        return $this->Get(self::MENU_ITEM_ID);
    }

    public function SetMenuItemOptionId($value) {
        $this->Set(self::MENU_ITEM_OPTION_ID, $value);
    }

    /**
     * @var integer
     */
    public function GetMenuItemOptionId() {
        return $this->Get(self::MENU_ITEM_OPTION_ID);
    }

    public function SetUserId($value) {
        $this->Set(self::USER_ID, $value);
    }

    /**
     * @var integer
     */
    public function GetUserId() {
        return $this->Get(self::USER_ID);
    }

    public function SetOrderId($value) {
        $this->Set(self::ORDER_ID, $value);
    }

    /**
     * @var integer
     */
    public function GetOrderId() {
        return $this->Get(self::ORDER_ID);
    }

    
    public function SetOrderItemId($value) {
        $this->Set(self::ORDER_ITEM_ID, $value);
    }

    /**
     * @var integer
     */
    public function GetOrderItemId() {
        return $this->Get(self::ORDER_ITEM_ID);
    }

    public function SetMenuItemSupplementId($value) {
        $this->Set(self::MENU_ITEM_SUPPLEMENT_ID, $value);
    }

    /**
     * @var integer
     */
    public function GetMenuItemSupplementId() {
        return $this->Get(self::MENU_ITEM_SUPPLEMENT_ID);
    }
    
    public function SetMonth($value) {
        $this->Set(self::MONTH, $value);
    }

    /**
     * @var integer
     */
    public function GetMonth() {
        return $this->Get(self::MONTH);
    }

}
