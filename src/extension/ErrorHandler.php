<?php

namespace extension;

class ErrorHandler {
    public function __invoke($request, $response, $exception) {
       // var_dump($exception);
        return $response
                ->withHeader("Access-Control-Allow-Origin", "*")
                ->withHeader("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, Accept, Origin, Authorization")
                ->withHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, PATCH, OPTIONS")
                ->withHeader("Content-Type", "application/json")
                ->withStatus(500)
                ->write('{"error":"'. $exception->getMessage() . '"}');
    /*Something went wrong! */
        
    }
 }


class NotFoundErrorHandler{
    public function __invoke($request, $response) {
        return $response
                ->withHeader("Access-Control-Allow-Origin", "*")
                ->withHeader("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, Accept, Origin, Authorization")
                ->withHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, PATCH, OPTIONS")
                ->withHeader("Content-Type", "application/json")
                ->withStatus(404)
                ->withHeader('Content-Type', 'application/json')
                ->write('{"error":"Resource not found."}');
        
    }

}