<?php

namespace config;

class DB{

  // AWARDSPACE
   /*
   private static $host="pdb22.awardspace.net";
   private static $port=3306;
   private static $socket="";
   private static $user="2484732_eaton";
   private static $password="someeatonpassword123";
   private static $dbname="2484732_eaton";
   */

  //lOCAL
  private static $host="127.0.0.1";
  private static $port=3306;
  private static $socket="";
  private static $user="root";
  private static $password="";
  private static $dbname="EatOn";

  /*
  public static $con;

  public static function Initialize(){
    self::$con = new mysqli(self::$host, self::$user, self::$password, self::$dbname, self::$port, self::$socket)
    	or die ('Could not connect to the database server' . mysqli_connect_error());

    self::$con->set_charset("utf8");
  }*/

  
    const COMPANY_TABLE = "company";
    const MENU_TABLE = "menu";
    const MENU_ITEM_TABLE = "menu_item";
    const MENU_ITEM_OPTION_TABLE = "menu_item_option";
    const MENU_ITEM_OPTION_SUPPLEMENT_TABLE = "menu_item_option_supplement";
    const OPENING_HOUR_TABLE = "opening_hour";
    const ORDER_TABLE = "`order`";
    const ORDER_ITEM_TABLE = "order_item";
    const ORDER_ITEM_SUPPLEMENT_TABLE = "order_item_supplement";
    const RESERVATION_TABLE = "reservation";
    const USER_TABLE = "`user`";
  
  /*
    const COMPANY_TABLE                     = "eaton_company";
    const MENU_TABLE                        = "eaton_menu";
    const MENU_ITEM_TABLE                   = "eaton_menu_item";
    const MENU_ITEM_OPTION_TABLE            = "eaton_menu_item_option";
    const MENU_ITEM_OPTION_SUPPLEMENT_TABLE = "eaton_menu_item_option_supplement";
    const OPENING_HOUR_TABLE                = "eaton_opening_hour";
    const ORDER_TABLE                       = "eaton_order";
    const ORDER_ITEM_TABLE                  = "eaton_order_item";
    const ORDER_ITEM_SUPPLEMENT_TABLE       = "eaton_order_item_supplement";
    const RESERVATION_TABLE                 = "eaton_reservation";
    const USER_TABLE                        = "eaton_user";
  */
    
 public function connect(){
    $con = new \mysqli(DB::$host, DB::$user, DB::$password, DB::$dbname, DB::$port, DB::$socket)
    or die ('Could not connect to the database server' . mysqli_connect_error());

    $con->set_charset("utf8");
    
    return $con;

 }


}
