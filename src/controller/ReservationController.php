<?php

namespace controller;

use controller\BaseController;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use db\DBReservation as DBReservation;
use model\Reservation as Reservation;
use \shared\QueryOptions as QueryOptions;

class ReservationController extends BaseController {

    public function getAll(Request $request, Response $response, array $args) {

        $queryOptions = $this->buildQueryOptions($request);

        $all = DBReservation::GetAll($this->db, $queryOptions);

        //self::buildAllWithQueryParams($this->db, $all, $this->getQueryParams(), $queryOptions);

        $response->getBody()->write(Reservation::AllToJSON($all));

        return $response;
    }

    public function get($request, $response, array $args) {

        //$id = $request->getAttribute("id");

        $queryOptions = $this->buildQueryOptions($request);

        $item = DBReservation::GetById($this->db, $queryOptions);

        if ($item != null) {
            //self::buildWithQueryParams($this->db, $item, $this->getQueryParams(), $queryOptions);
            $response->getBody()->write($item->ToJSON());
        } else {
            $response->getBody()->write("{}");
        }

        return $response;
    }

    public function add($request, $response, array $args) {

        $id = $request->getParam("id");
        $userId = $request->getParam("userId");
        $companyId = $request->getParam("companyId");
        $date = $request->getParam("date");
        $status = $request->getParam("status");
        $remark = $request->getParam("remark");

        $item = new Reservation($id, $userId, $companyId, $date, $status, $remark);

        DBReservation::Save($this->db, $item, $this->user);

        $response->getBody()->write($item->ToJSON());

        return $response;
    }

    public function update($request, $response, array $args) {

        $id = $request->getAttribute("id");

        $userId = $request->getParam("userId");
        $companyId = $request->getParam("companyId");
        $date = $request->getParam("date");
        $status = $request->getParam("status");
        $remark = $request->getParam("remark");

        $item = new Reservation($id, $userId, $companyId, $date, $status, $remark);

        DBReservation::Update($this->db, $item, $this->user);

        $response->getBody()->write($item->ToJSON());

        return $response;
    }

    public function delete($request, $response, array $args) {

        //$id = $request->getAttribute("id");

        DBReservation::Delete($this->db, $this->buildQueryOptions($request), $this->user);

        $response->getBody()->write("{}");

        return $response;
    }

    public static function buildWithQueryParams($db, Reservation $reservation, $qryParms, QueryOptions $queryOptions) {

    }

}
