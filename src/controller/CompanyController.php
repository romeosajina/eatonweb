<?php

namespace controller;

use controller\BaseController;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use db\DBCompany as DBCompany;
use model\Company as Company;
use \shared\QueryOptions as QueryOptions;

class CompanyController extends BaseController {
    
    public function __construct(\Slim\Container $container) {
        
        //Ako se dodaje novi (registracija), onda preskoci validaciju user-a
        if($container->get("request")->getMethod() == "POST")
            $this->initDB();        
        else
            parent::__construct($container);
                        
    }
    
    
    public function getAll(Request $request, Response $response, array $args) {

        $queryOptions = $this->buildQueryOptions($request);

        $all = DBCompany::GetAll($this->db, $queryOptions);

        self::buildAllWithQueryParams($this->db, $all, $this->getQueryParams(), $queryOptions);

        $response->getBody()->write(Company::AllToJSON($all));

        return $response;
    }

    public function get($request, $response, array $args) {

        $queryOptions = $this->buildQueryOptions($request);

        $item = DBCompany::GetById($this->db, $queryOptions);

        if ($item != null) {

            self::buildWithQueryParams($this->db, $item, $this->getQueryParams(), $queryOptions);
            $response->getBody()->write($item->ToJSON());
        
        } else {

            $response->getBody()->write("{}");
        }

        return $response;
    }

    public function add($request, $response, array $args) {

        $id = $request->getParam("id");
        $name = $request->getParam("name");
        $city = $request->getParam("city");
        $address = $request->getParam("address");
        $latitude = $request->getParam("latitude");
        $longitude = $request->getParam("longitude");
        $photo = $request->getParam("photo");
        $phone = $request->getParam("phone");
        $delivery = $request->getParam("delivery");
        $pickUp = $request->getParam("pickUp");
        $minDeliveryPrice = $request->getParam("minDeliveryPrice");
        $maxDestinationRange = $request->getParam("maxDestinationRange");
        $maxFreeDeliveryRange = $request->getParam("maxFreeDeliveryRange");
        $description = $request->getParam("description");   
        $ethAddress = $request->getParam("ethAddress");

        $email = $request->getParam("email");
        $password = $request->getParam("password");

        $item = new Company($id, $name, $city, $address, $latitude, $longitude, $photo, $phone, $delivery, $pickUp, $minDeliveryPrice, $maxDestinationRange, $maxFreeDeliveryRange, $ethAddress, $description);
        
        $item->SetEmail($email);
        $item->SetPassword($password);
        
        DBCompany::Save($this->db, $item, $this->user);

        $response->getBody()->write($item->ToJSON());

        return $response;
    }

    public function update($request, $response, array $args) {
        
        $id = $request->getAttribute("id");

        $name = $request->getParam("name");
        $city = $request->getParam("city");
        $address = $request->getParam("address");
        $latitude = $request->getParam("latitude");
        $longitude = $request->getParam("longitude");
        $photo = $request->getParam("photo");
        $phone = $request->getParam("phone");
        $delivery = $request->getParam("delivery");
        $pickUp = $request->getParam("pickUp");
        $minDeliveryPrice = $request->getParam("minDeliveryPrice");
        $maxDestinationRange = $request->getParam("maxDestinationRange");
        $maxFreeDeliveryRange = $request->getParam("maxFreeDeliveryRange");
        $description = $request->getParam("description");
        $ethAddress = $request->getParam("ethAddress");
        
        $item = new Company($id, $name, $city, $address, $latitude, $longitude, $photo, $phone, $delivery, $pickUp, $minDeliveryPrice, $maxDestinationRange, $maxFreeDeliveryRange, $ethAddress, $description);

        DBCompany::Update($this->db, $item, $this->user);

        $response->getBody()->write($item->ToJSON());

        return $response;
    }

    public function delete($request, $response, array $args) {
        
        $qo = $this->buildQueryOptions($request);
        
        if($this->user->GetId() != $qo->GetId())
            throw new \Exception ("Unauthorized access");
            
        DBCompany::Delete($this->db, $qo, $this->user);

        $response->getBody()->write("{}");

        return $response;
    }

    public static function buildWithQueryParams($db, Company $company, $qryParms, QueryOptions $queryOptions) {


        if (self::isCollectionExpanded("menus", $qryParms)) {

            $qryParms = self::removeCollectionExpanded("menus", $qryParms);

            $queryOptions->SetCompanyId($company->GetId());

            $company->SetMenus(\db\DBMenu::GetAll($db, $queryOptions));
        
            if (MenuController::needToPropagateBuild($qryParms)) {
                MenuController::buildAllWithQueryParams($db, $company->GetMenus(), $qryParms, $queryOptions);
            }
            
        }

        if (self::isCollectionExpanded("openingHours", $qryParms)) {

            $qryParms = self::removeCollectionExpanded("openingHours", $qryParms);

            $queryOptions->SetCompanyId($company->GetId());

            $company->SetOpeningHours(\db\DBOpeningHour::GetAll($db, $queryOptions));            
        }
        
    }

}
