<?php

namespace controller;

use controller\BaseController;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use db\DBMenuItemOption as DBMenuItemOption;
use model\MenuItemOption as MenuItemOption;
use \shared\QueryOptions as QueryOptions;

class MenuItemOptionController extends BaseController {

    public function getAll(Request $request, Response $response, array $args) {

        $queryOptions = $this->buildQueryOptions($request);

        $all = DBMenuItemOption::GetAll($this->db, $queryOptions);

        self::buildAllWithQueryParams($this->db, $all, $this->getQueryParams(), $queryOptions);

        $response->getBody()->write(MenuItemOption::AllToJSON($all));

        return $response;
    }

    public function get($request, $response, array $args) {

        $queryOptions = $this->buildQueryOptions($request);

        $item = DBMenuItemOption::GetById($this->db, $queryOptions);

        if ($item != null) {
            
            self::buildWithQueryParams($this->db, $item, $this->getQueryParams(), $queryOptions);
            $response->getBody()->write($item->ToJSON());
       
        } else {

            $response->getBody()->write("{}");
        
        }

        return $response;
    }

    public function add($request, $response, array $args) {

        $id = $request->getParam("id");
        $menuItemId = $request->getAttribute("menuItemId");//$request->getParam("menuItemId");
        $name = $request->getParam("name");
        $price = $request->getParam("price");

        $item = new MenuItemOption($id, $menuItemId, $name, $price);

        DBMenuItemOption::Save($this->db, $item, $this->user);

        $response->getBody()->write($item->ToJSON());

        return $response;
    }

    public function update($request, $response, array $args) {

        $id = $request->getAttribute("id");

        $menuItemId = $request->getParam("menuItemId");
        $name = $request->getParam("name");
        $price = $request->getParam("price");

        $item = new MenuItemOption($id, $menuItemId, $name, $price);

        DBMenuItemOption::Update($this->db, $item, $this->user);

        $response->getBody()->write($item->ToJSON());

        return $response;
    }

    public function delete($request, $response, array $args) {

        DBMenuItemOption::Delete($this->db, $this->buildQueryOptions($request), $this->user);

        $response->getBody()->write("{}");

        return $response;
    }

    public static function buildWithQueryParams($db, MenuItemOption $menuItemOption, $qryParms, QueryOptions $queryOptions) {


        if (self::isCollectionExpanded("menuItemOptionSupplements", $qryParms)) {

            $qryParms = self::removeCollectionExpanded("menuItemOptionSupplements", $qryParms);

            $queryOptions->SetMenuItemOptionId($menuItemOption->GetId());

            $menuItemOption->SetMenuItemOptionSupplement(\db\DBMenuItemOptionSupplement::GetAll($db, $queryOptions));
        }
    }

    public static function needToPropagateBuild($qryParms) {
        return self::isCollectionExpanded("menuItemOptionSupplements", $qryParms);
    }

}
