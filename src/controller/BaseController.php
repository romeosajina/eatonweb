<?php

namespace controller;

use config\DB;
use \Slim\Container;
use model\AppUser;
use \mysqli as mysqli;
use \shared\QueryOptions as QueryOptions;
use \Psr\Http\Message\ServerRequestInterface as Request;


class BaseController {
    
    /**
     * @var AppUser
     */
    protected $user = null;
   
    /**
     * @var mysqli
     */
    protected $db = null;
    
    /**
     * @var Container
     */
    protected $container;
    
    protected $inBatch = false;

    public function __construct(/* ContainerInterface */Container $container) {
        $this->container = $container;

        /* Ako se koristi batch request onda se jednom postavlja user i konekcija kod instancijanja batch request controllera pa se u ostalim samo koriste */
        $ru = $this->container->offsetExists('requestUser');
        if($ru){
            $this->user = $this->container->get('requestUser');
            $this->db = $this->container->get('requestDb');           
            $this->inBatch = true;
            
        }else if(!$this->inBatch){ //Ako nije u batch modu - može se nadjačat kontruktor i postavit da je u batch modu da se korisnik ne validira
            
            $this->initDB();
            $this->authUser();
        }
        
    }
    
    public function authUser(){
   
        //var_dump($this->getRequest());
        
        //Za potrebe testiranja
       if($this->getRequest()->getParam("dc")){
           $qo = new QueryOptions();
           $qo->SetId(1);
           $this->user = \db\DBCompany::GetById($this->db, $qo);   
           return;
       }
       if($this->getRequest()->getParam("du")){
           $qo = new QueryOptions();
           $qo->SetId(1);
           $this->user = \db\DBUser::GetById($this->db, $qo);   
           return;
       }
        
        
               
        $u = $this->getRequest()->getHeader('PHP_AUTH_USER');
        $p = $this->getRequest()->getHeader('PHP_AUTH_PW');
        
        $uH = $this->getRequest()->getHeader('App-User-Is-Company');
        
        $userIsCompany = $uH != null && isset($uH[0]) && $uH[0] == "true";
        
        //echo $u[0]. " ". $p[0];
        
        if($p != null && isset($p[0]) && $u != null && isset($u[0])){       
            
            if($userIsCompany)
                $this->user = \db\DBCompany::Authenticate($this->db, $u[0], $p[0]);            
            else 
                $this->user = \db\DBUser::Authenticate($this->db, $u[0], $p[0]);            

        }else{
           throw new \Exception ('Authentification required. Please provide reuqired headers');

        }
        
        if($this->user == null)
            throw new \Exception ('Authentification filed due to wrong creditientals');
        
        //var_dump($this->user);    
    }


    public function initDB(){
        $db = new DB();
        $this->db = $db->connect();
        /*
        $this->db->autocommit(FALSE);
        
        if ($result = $this->db->query("SELECT @@autocommit")) {
            $row = $result->fetch_row();
            printf("Autocommit is %s\n", $row[0]);
            $result->free();
        }*/
    }
    

    function __destruct() {
        //Ako se ne koristi u batchu onda zatvori konekciju
        if($this->inBatch == false)
            $this->db->close();
    
        $this->user = null;
    }

    public function getQueryParams($key = null) {

       // $params = $this->container["request"]->getQueryParams();
         $params = $this->getRequest()->getQueryParams();

        //$params = $request->getQueryParams();

        $expand = array();

        if (!empty($params["expand"]))
            $expand = explode(",", $params["expand"]);


        if ($key == "expand")
            return $expand;


        $params["expand"] = $expand;

        return $params;
    }

    /**
     * @var Request
     */
    public function getRequest() {
        //return $this->container["request"];
        return $this->container->get("request");
    }
    
    
    protected function buildQueryOptions(Request $request/*= NULL*/){
        
        if($request == NULL)
            $request = $this->getRequest();
        
        /* @var $request Request */
        
        $qo = new QueryOptions();

        foreach (QueryOptions::ALL_ATTRIBUTES as $attr){
            //echo 'set ' .$attr . " = " . $request->getAttribute($attr);
            $qo->Set($attr, $request->getAttribute($attr));
        }
        
        
        $qp = $request->getQueryParams();
        if($qp != NULL && isset($qp["deleteAllConnectedChildren"]) && $qp["deleteAllConnectedChildren"] == "true")
            $qo->SetDeleteAllConnectedChildren(TRUE);
            
        if($qp != NULL && isset($qp["onlyDoneOrders"]) && $qp["onlyDoneOrders"] == "true")
          $qo->SetOnlyDoneOrders(TRUE);
        
        if($qp != NULL && isset($qp["onlyDoneOrders"]) && $qp["onlyDoneOrders"] == "false")
          $qo->SetOnlyDoneOrders(FALSE);

        return $qo;
    }

    public static function buildAllWithQueryParams($db, $items, $qryParms, QueryOptions $queryOptions) {
        $class = get_called_class();
        //var_dump($class);
        foreach ($items as $item) {
            $class::buildWithQueryParams($db, $item, $qryParms, $queryOptions);
        }
    }

    public static function buildWithQueryParams($db, $item, $qryParms, QueryOptions $queryOptions) {
     
    }
    
    public static function isCollectionExpanded($name, $qryParms){
        return in_array($name, $qryParms["expand"]);
    }
    
    public static function removeCollectionExpanded($name, $qryParms){
        
        //$qryParms["expand"][$name] = NULL;
         
        unset($qryParms["expand"][$name]);

        return $qryParms;
    }    

    public static function needToPropagateBuild($qryParms){
        return count($qryParms["expand"]) > 0;
    }

}