<?php

namespace controller;

use controller\BaseController;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use db\DBMenu as DBMenu;
use model\Menu as Menu;
use \shared\QueryOptions as QueryOptions;

class MenuController extends BaseController {

    public function getAll(Request $request, Response $response, array $args) {

        $queryOptions = $this->buildQueryOptions($request);

        $all = DBMenu::GetAll($this->db, $queryOptions);

        self::buildAllWithQueryParams($this->db, $all, $this->getQueryParams(), $queryOptions);

        $response->getBody()->write(Menu::AllToJSON($all));

        return $response;
    }

    public function get(Request $request, Response $response, array $args) {

        $queryOptions = $this->buildQueryOptions($request);

        $item = DBMenu::GetById($this->db, $queryOptions);

        if ($item != null) {
            
            self::buildWithQueryParams($this->db, $item, $this->getQueryParams(), $queryOptions);
            $response->getBody()->write($item->ToJSON());
            
        } else {
            
            $response->getBody()->write("{}");
        
        }

        return $response;
    }

    public function add(Request $request, Response $response, array $args) {

        $id = $request->getParam("id");
        $companyId = $request->getAttribute("companyId");//$request->getParam("companyId");
        $type = $request->getParam("type");
        $photo = $request->getParam("photo");

        $item = new Menu($id, $companyId, $type, $photo);

        DBMenu::Save($this->db, $item, $this->user);

        $response->getBody()->write($item->ToJSON());

        return $response;
    }

    public function update(Request $request, Response $response, array $args) {

        $id = $request->getAttribute("id");

        $companyId = $request->getParam("companyId");
        $type = $request->getParam("type");
        $photo = $request->getParam("photo");

        $item = new Menu($id, $companyId, $type, $photo);

        DBMenu::Update($this->db, $item, $this->user);

        $response->getBody()->write($item->ToJSON());

        return $response;
    }

    public function delete(Request $request, Response $response, array $args) {

        DBMenu::Delete($this->db, $this->buildQueryOptions($request), $this->user);

        $response->getBody()->write("{}");

        return $response;
    }

    public static function buildWithQueryParams($db, Menu $menu, $qryParms, QueryOptions $queryOptions) {

        if (self::isCollectionExpanded("menuItems", $qryParms)) {

            $qryParms = self::removeCollectionExpanded("menuItems", $qryParms);

            $queryOptions->SetMenuId($menu->GetId());

            $menu->SetMenuItems(\db\DBMenuItem::GetAll($db, $queryOptions));
            
            if (MenuItemController::needToPropagateBuild($qryParms)) {
                MenuItemController::buildAllWithQueryParams($db, $menu->GetMenuItems(), $qryParms, $queryOptions);
            }
        }

    }

    public static function needToPropagateBuild($qryParms) {
        return self::isCollectionExpanded("menuItems", $qryParms);
    }

}
