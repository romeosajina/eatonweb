<?php

namespace controller;

use controller\BaseController;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use db\DBMenuItemOptionSupplement as DBMenuItemOptionSupplement;
use model\MenuItemOptionSupplement as MenuItemOptionSupplement;
use \shared\QueryOptions as QueryOptions;

class MenuItemOptionSupplementController extends BaseController {

    public function getAll(Request $request, Response $response, array $args) {

        $queryOptions = $this->buildQueryOptions($request);

        $all = DBMenuItemOptionSupplement::GetAll($this->db, $queryOptions);

        //self::buildAllWithQueryParams($this->db, $all, $this->getQueryParams(), $queryOptions);

        $response->getBody()->write(MenuItemOptionSupplement::AllToJSON($all));

        return $response;
    }

    public function get($request, $response, array $args) {

        //$id = $request->getAttribute("id");

        $queryOptions = $this->buildQueryOptions($request);

        $item = DBMenuItemOptionSupplement::GetById($this->db, $queryOptions);

        if ($item != null) {
            //self::buildWithQueryParams($this->db, $item, $this->getQueryParams(), $queryOptions);
            $response->getBody()->write($item->ToJSON());
        } else {
            $response->getBody()->write("{}");
        }

        return $response;
    }

    public function add($request, $response, array $args) {

        $id = $request->getParam("id");
        $menuItemOptionId = $request->getAttribute("menuItemOptionId");//$request->getParam("menuItemOptionId");
        $name = $request->getParam("name");
        $price = $request->getParam("price");
        $photo = $request->getParam("photo");

        $item = new MenuItemOptionSupplement($id, $menuItemOptionId, $name, $price, $photo);

        DBMenuItemOptionSupplement::Save($this->db, $item, $this->user);

        $response->getBody()->write($item->ToJSON());

        return $response;
    }

    public function update($request, $response, array $args) {

        $id = $request->getAttribute("id");

        $menuItemOptionId = $request->getParam("menuItemOptionId");
        $name = $request->getParam("name");
        $price = $request->getParam("price");
        $photo = $request->getParam("photo");

        $item = new MenuItemOptionSupplement($id, $menuItemOptionId, $name, $price, $photo);

        DBMenuItemOptionSupplement::Update($this->db, $item, $this->user);

        $response->getBody()->write($item->ToJSON());

        return $response;
    }

    public function delete($request, $response, array $args) {

        DBMenuItemOptionSupplement::Delete($this->db, $this->buildQueryOptions($request), $this->user);

        $response->getBody()->write("{}");

        return $response;
    }

    public static function buildWithQueryParams($db, MenuItemOptionSupplement $menuItemOptionSupplement, $qryParms, QueryOptions $queryOptions) {
    }

}
