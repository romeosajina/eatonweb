<?php

namespace controller;

use controller\BaseController;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use db\DBUser as DBUser;
use model\User as User;
use \shared\QueryOptions as QueryOptions;

class UserController extends BaseController {
    
    public function __construct(\Slim\Container $container) {
        
        //Ako se dodaje novi (registracija), onda preskoci validaciju user-a
        if($container->get("request")->getMethod() == "POST")
            $this->initDB();        
        else
            parent::__construct($container);
                        
    }

    
    public function getAll(Request $request, Response $response, array $args) {

        $queryOptions = $this->buildQueryOptions($request);

        $all = DBUser::GetAll($this->db, $queryOptions);

        //self::buildAllWithQueryParams($this->db, $all, $this->getQueryParams(), $queryOptions);

        $response->getBody()->write(User::AllToJSON($all));

        return $response;
    }

    public function get($request, $response, array $args) {

        $queryOptions = $this->buildQueryOptions($request);

        $item = DBUser::GetById($this->db, $queryOptions);

        if ($item != null) {
            //self::buildWithQueryParams($this->db, $item, $this->getQueryParams(), $queryOptions);
            $response->getBody()->write($item->ToJSON());
        } else {
            $response->getBody()->write("{}");
        }

        return $response;
    }

    public function add($request, $response, array $args) {

        $id = $request->getParam("id");
        $name = $request->getParam("name");
        $surname = $request->getParam("surname");
        $city = $request->getParam("city");
        $address = $request->getParam("address");
        $latitude = $request->getParam("latitude");
        $longitude = $request->getParam("longitude");
        $phone = $request->getParam("phone");
        $showNumber = $request->getParam("showNumber");
        
        $email = $request->getParam("email");
        $password = $request->getParam("password");


        $item = new User($id, $name, $surname, $city, $address, $latitude, $longitude, $phone, $showNumber);
        
        $item->SetEmail($email);
        $item->SetPassword($password);
        
        DBUser::Save($this->db, $item, $this->user);

        $response->getBody()->write($item->ToJSON());

        return $response;
    }

    public function update($request, $response, array $args) {

        $id = $request->getAttribute("id");

        $name = $request->getParam("name");
        $surname = $request->getParam("surname");
        $city = $request->getParam("city");
        $address = $request->getParam("address");
        $latitude = $request->getParam("latitude");
        $longitude = $request->getParam("longitude");
        $phone = $request->getParam("phone");
        $showNumber = $request->getParam("showNumber");

        $item = new User($id, $name, $surname, $city, $address, $latitude, $longitude, $phone, $showNumber);

        DBUser::Update($this->db, $item, $this->user);

        $response->getBody()->write($item->ToJSON());

        return $response;
    }

    public function delete($request, $response, array $args) {

        $qo = $this->buildQueryOptions($request);
        
        if($this->user->GetId() != $qo->GetId())
            throw new \Exception ("Unauthorized access");
        
        DBUser::Delete($this->db, $qo, $this->user);

        $response->getBody()->write("{}");

        return $response;
    }

    public static function buildWithQueryParams($db, User $user, $qryParms, QueryOptions $queryOptions) {

        /*
          if (self::isCollectionExpanded("menus")) {

          $qryParms = self::removeCollectionExpanded("menus");

          $queryOptions->SeUserId($user->GetId());

          $user->SetMenus(DBMenu::GetAll($db, $queryOptions));
          }

          if (self::needToPropagateBuild($qryParms)) {
          MenuController::buildAllWithQueryParams($db, $company->GetMenus(), $qryParms, $queryOptions);
          }
         */
    }

}
