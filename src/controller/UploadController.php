<?php

namespace controller;

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\UploadedFile;
use controller\BaseController;
//You either did not specify a file to upload or are trying to upload a file to a protected or nonexistent location.
class UploadController extends BaseController {

    public function upload(Request $request, Response $response, array $args) {

        $fileNames = json_decode($request->getParam("fileNames"));
        
        $customDirectory = $request->getParam("destination");

        $uploadedFiles = $request->getUploadedFiles();
        //$directory = $request->getParam("destination");
        //$directory = getcwd().DIRECTORY_SEPARATOR."files";
        $directory = "files";

        if($customDirectory != null && $customDirectory != '')
            $directory .= "/" . $customDirectory;
            //DIRECTORY_SEPARATOR
        
        foreach ($fileNames as $key) {

            if ($uploadedFiles[$key]->getError() === UPLOAD_ERR_OK) {
                $filename = $this->moveUploadedFile($directory, $uploadedFiles[$key]);
                /*
                $resp = "{";
                $resp .= '"name": "'.$filename.'",';
                $resp .= '"path": "'.$directory.'",';
                $resp .= '"error": null';
                $resp .= "}";
                $response->getBody()->write('{"'.$key.'":'.$resp.'}');
                */
                
                $resp = new \stdClass();
                $resp->name = $filename;
                $resp->path = $directory;
                $resp->error = null;
                      
                $response->getBody()->write('{"'.$key.'":'.json_encode($resp, JSON_UNESCAPED_UNICODE).'}');
            }
        }

        //var_dump($request);    
        //var_dump($fileNames);
        //var_dump($uploadedFiles);
        //$response->getBody()->write('{}');

        return $response;
    }

    private function moveUploadedFile($directory, UploadedFile $uploadedFile) {

         if(!is_dir($directory)) 
            mkdir($directory, 0777, TRUE);
        
        $extension = \pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
        //$basename = \bin2hex(\random_bytes(8));
        //$basename = \basename($uploadedFile->getClientFilename());
        $basename = \pathinfo($uploadedFile->getClientFilename(), PATHINFO_FILENAME);
        $filename = \sprintf('%s.%0.8s', $basename, $extension);
    

        $filename = "_app_" . str_replace(" ","_", $filename);       
        
        
        
        while(file_exists($directory . DIRECTORY_SEPARATOR . $filename)){
            $filename = $this->generateUniqueName($filename);
        }
       
        
        
        $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);

        return $filename;
    }

    
    
    
    
    
    private function generateUniqueName($string) {

        $number = "";
        while (strlen($string) > 0 && is_numeric($string[0])) {

            $number .= $string[0];

            $string = substr($string, 1, strlen($string));
        }
        $number = $number == "" ? -1 : intval($number);

        return ++$number . $string;
    }

}
