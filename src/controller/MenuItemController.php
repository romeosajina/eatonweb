<?php

namespace controller;

use controller\BaseController;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use db\DBMenuItem as DBMenuItem;
use model\MenuItem as MenuItem;
use \shared\QueryOptions as QueryOptions;

class MenuItemController extends BaseController {

    public function getAll(Request $request, Response $response, array $args) {

        $queryOptions = $this->buildQueryOptions($request);

        $all = DBMenuItem::GetAll($this->db, $queryOptions);

        self::buildAllWithQueryParams($this->db, $all, $this->getQueryParams(), $queryOptions);

        $response->getBody()->write(MenuItem::AllToJSON($all));

        return $response;
    }

    public function get(Request $request, Response $response, array $args) {

        $queryOptions = $this->buildQueryOptions($request);

        $item = DBMenuItem::GetById($this->db, $queryOptions);

        if ($item != null) {
            
            self::buildWithQueryParams($this->db, $item, $this->getQueryParams(), $queryOptions);
            $response->getBody()->write($item->ToJSON());
 
        } else {
        
            $response->getBody()->write("{}");
        }

        return $response;
    }

    public function add(Request $request, Response $response, array $args) {

        $id = $request->getParam("id");
        $menuId = $request->getAttribute("menuId");//$request->getParam("menuId");
        $name = $request->getParam("name");
        $price = $request->getParam("price");
        $rate = $request->getParam("rate");
        $photo = $request->getParam("photo");
        $discount = $request->getParam("discount");
        $description = $request->getParam("description");
       
        $item = new MenuItem($id, $menuId, $name, $price, $rate, $photo, $discount, $description);

        DBMenuItem::Save($this->db, $item, $this->user);

        $response->getBody()->write($item->ToJSON());

        return $response;
    }

    public function update(Request $request, Response $response, array $args) {

        $id = $request->getAttribute("id");

        $menuId = $request->getAttribute("menuId");//$request->getParam("menuId");
        $name = $request->getParam("name");
        $price = $request->getParam("price");
        $rate = $request->getParam("rate");
        $photo = $request->getParam("photo");
        $discount = $request->getParam("discount");
        $description = $request->getParam("description");

        $item = new MenuItem($id, $menuId, $name, $price, $rate, $photo, $discount, $description);

        DBMenuItem::Update($this->db, $item, $this->user);

        $response->getBody()->write($item->ToJSON());

        return $response;
    }

    public function delete(Request $request, Response $response, array $args) {

        DBMenuItem::Delete($this->db, $this->buildQueryOptions($request), $this->user);

        $response->getBody()->write("{}");

        return $response;
    }

    public static function buildWithQueryParams($db, MenuItem $menuItem, $qryParms, QueryOptions $queryOptions) {

        
        if (self::isCollectionExpanded("menuItemOptions", $qryParms)) {

            $qryParms = self::removeCollectionExpanded("menuItemOptions", $qryParms);

            $queryOptions->SetMenuItemId($menuItem->GetId());

            $menuItem->SetMenuItemOptions(\db\DBMenuItemOption::GetAll($db, $queryOptions));
         
            if(MenuItemOptionController::needToPropagateBuild($qryParms)) {
                MenuItemOptionController::buildAllWithQueryParams($db, $menuItem->GetMenuItemOptions(), $qryParms, $queryOptions);
            }
        }
  
    }

    public static function needToPropagateBuild($qryParms) {
        return self::isCollectionExpanded("menuItemOptions", $qryParms);
    }

}
