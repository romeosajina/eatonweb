<?php

namespace controller;

use controller\BaseController;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use db\DBOrder as DBOrder;
use model\Order as Order;
use \shared\QueryOptions as QueryOptions;
use \services\PusherService as PusherService;

class OrderController extends BaseController {

    public function getAll(Request $request, Response $response, array $args) {
       
        $queryOptions = $this->buildQueryOptions($request);
        
        if($this->user->isCompany())
            $queryOptions->SetCompanyId($this->user->GetId());
        else
            $queryOptions->SetUserId($this->user->GetId());
        
    
        $all = DBOrder::GetAll($this->db, $queryOptions);

        self::buildAllWithQueryParams($this->db, $all, $this->getQueryParams(), $queryOptions);
        
        $response->getBody()->write(Order::AllToJSON($all));

        return $response;
    }

    public function get($request, $response, array $args) {
        
        $queryOptions = $this->buildQueryOptions($request);

        $item = DBOrder::GetById($this->db, $queryOptions);
        
        if ($item != null) {
            
            if($this->user->isCompany() && $this->user->GetId() != $item->GetCompanyId() ||
               $this->user->isUser() && $this->user->GetId() != $item->GetUserId())
                throw new \Exception ("Unauthorized access");
            
            
            self::buildWithQueryParams($this->db, $item, $this->getQueryParams(), $queryOptions);
            $response->getBody()->write($item->ToJSON());
        } else {
            $response->getBody()->write("{}");
        }

        return $response;
    }

    public function getUser($request, $response, array $args) {

        $queryOptions = $this->buildQueryOptions($request);

        $item = \db\DBUser::GetById($this->db, $queryOptions);

        if ($item != null) {
            $response->getBody()->write($item->ToJSON());
        } else {
            $response->getBody()->write("{}");
        }

        return $response;
    }

    public function add($request, $response, array $args) {

        $id = $request->getParam("id");
        $userId = $request->getParam("userId");
        $companyId = $request->getParam("companyId");
        $date = $request->getParam("date");
        $deliveryTime = $request->getParam("deliveryTime");
        $status = $request->getParam("status");
        $rate = $request->getParam("rate");
        $remark = $request->getParam("remark");
        $paymentMethod = $request->getParam("paymentMethod");

        $item = new Order($id, $userId, $companyId, $date, $deliveryTime, $status, $rate, $remark, $paymentMethod);

        DBOrder::Save($this->db, $item, $this->user);
        
        PusherService::OrderAdded($item);

        $response->getBody()->write($item->ToJSON());

        return $response;
    }

    public function update($request, $response, array $args) {

        $id = $request->getAttribute("id");

        $userId = $request->getParam("userId");
        $companyId = $request->getParam("companyId");
        $date = $request->getParam("date");
        $deliveryTime = $request->getParam("deliveryTime");
        $status = $request->getParam("status");
        $rate = $request->getParam("rate");
        $remark = $request->getParam("remark");
        $paymentMethod = $request->getParam("paymentMethod");

        $item = new Order($id, $userId, $companyId, $date, $deliveryTime, $status, $rate, $remark, $paymentMethod);

        DBOrder::Update($this->db, $item, $this->user);

        PusherService::OrderUpdated($item);

        $response->getBody()->write($item->ToJSON());

        return $response;
    }

    public function delete($request, $response, array $args) {

        //$id = $request->getAttribute("id");

        $qo = $this->buildQueryOptions($request);
        
        $order = DBOrder::GetById($this->db, $qo);
        
        DBOrder::Delete($this->db, $qo, $this->user);
        
        if($order != null)
            PusherService::OrderDeleted($order);

        $response->getBody()->write("{}");

        return $response;
    }

    public static function buildWithQueryParams($db, Order $order, $qryParms, QueryOptions $queryOptions) {
        
          if (self::isCollectionExpanded("orderItems", $qryParms)) {
              
            $qryParms = self::removeCollectionExpanded("orderItems", $qryParms);

            $queryOptions->SetOrderId($order->GetId());

            $order->SetOrderItems(\db\DBOrderItem::GetAll($db, $queryOptions));

            if (OrderItemController::needToPropagateBuild($qryParms)) {
              OrderItemController::buildAllWithQueryParams($db, $order->GetOrderItems(), $qryParms, $queryOptions);
            }

        }
        
    }

}
