<?php

namespace controller;

use controller\BaseController;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use services\PusherService as PusherService;

class BatchController extends BaseController {
    
    /** 
     * @var \Slim\App
     */
    private $app = null;


    public function __construct(\Slim\Container $container, \Slim\App $app) {
        parent::__construct($container);
        $this->app = $app;
    }
    
    
    public function process(Request $request, Response $response, array $args) {

        unset($this->container['errorHandler']); //Iskljuci standardni error handler
        unset($this->container['notFoundHandler']); //Iskljuci standardni error handler

        /* Postavljanje usera i konekcije u container da se ne ponovno kreiraju u requestovima */
        $this->container->offsetSet('requestUser', $this->user);
        $this->container->offsetSet('requestDb', $this->db);
        
        /* Ako se dogodi greška onda ništa nije spremljeno */
        $this->db->autocommit(FALSE);
        
        PusherService::SetInBatchMode();
               
        $parts = $request->getParam('parts');
        
        //Ako se desi greska onda ne spremaj
        $doCommit = true;
        
        //krsort($parts); // Key - DESC
        //ksort($parts); // Key - ASC
        
        $batchParts = array();
        foreach($parts as $part){ array_push($batchParts, new BatchPart($part)); }
        

        foreach($batchParts as $batchPart){
            /*@var $batchPart BatchPart */
            
            try{
                
                $relPath = $batchPart->GetPath($batchParts);
                $relPath = substr($relPath, strpos($relPath, 'server') + 6);

                $rps = $this->app->subRequest($batchPart->GetMethod(), $relPath, "", $request->getHeaders(), $request->getCookieParams(), json_encode($batchPart->GetPayload(), JSON_UNESCAPED_UNICODE), new \Slim\Http\Response());
     
                $batchPart->SetPayload(json_decode($rps->getBody(), JSON_UNESCAPED_UNICODE));
                  
            } catch (\Exception $e){
                $batchPart->SetError($e->getMessage() == "Identifier \"notFoundHandler\" is not defined."? "Resource not found." : $e->getMessage());
                $batchPart->SetPayload(null);
                $doCommit = FALSE;
                break;
            }
        }
        
        $allParts = new \stdClass();
        $allParts->parts = array_map(function ($item) { return $item->PrepareJSON();}, $batchParts);
        $response->getBody()->write(json_encode($allParts, JSON_PRETTY_PRINT));//JSON_UNESCAPED_UNICODE));
       

        $this->container->offsetSet('requestUser', null);
        $this->container->offsetSet('requestDb', null);
        
        /* Autocommit == FALSE pa je potrebno eksplicitno pozvati commit */
        if($doCommit){
            $this->db->commit();
            PusherService::BroadcastAll();
        }else{
            $this->db->rollback();
            PusherService::Clear();
        }
        
        //var_dump($parts);
            
        return $response;
    }
    
        
}



class BatchPart{
    
    private $id;
    private $method;
    private $path;
    private $payload;
    private $operation;
    private $parentPartId;
    private $error = null;

    public function __construct($part) {

        $this->method = 'GET';
        if($part['operation'] == 'create') $this->method = 'POST';
        if($part['operation'] == 'update') $this->method = 'PATCH';
        if($part['operation'] == 'delete') $this->method = 'DELETE';

        $this->id = $part["id"];
        $this->path = $part['path'];
        $this->payload = $part["payload"];
        $this->operation = $part['operation'];
        $this->parentPartId = isset($part['parentPartId'])? $part['parentPartId']: NULL;        
    }
    
    public function SetError($value){ $this->error = $value; }
    public function SetPayload($value){ $this->payload = $value; }
    public function GetError(){ return $this->error; }
    public function GetPayload(){ return $this->payload; }    
    public function GetMethod(){ return $this->method; }
    public function GetRequestPath(){ return $this->path; }
    
    
    private function buildPath($batchParts){
        
        $times = 0;
        $lastParentPartId = $this->parentPartId;
     
        while(strpos($this->path, "-1") !== false && $times++ < 100){

            $matchPart = NULL;
            
            foreach($batchParts as $batchPart){
                /*@var $batchPart BatchPart */

                if($batchPart->id == $lastParentPartId){
                    $matchPart = $batchPart;
                    break;
                }
            }

            if($matchPart == NULL) throw new \Exception("Matcher not found");

            $this->path = self::str_replace_last("-1", $matchPart->GetPayload()['id'], $this->path);
            
            $lastParentPartId = $matchPart->parentPartId;
        }
    }


    public function GetPath($batchParts){
        
        //$times = 0;
        
        //while(strpos($this->path, "-1") !== false && $times++ < 10) 
        //        $this->fixOne ($batchParts);
        
        $this->buildPath ($batchParts);
        
        return $this->path;
    }
/*
    private function fixOne($batchParts){
        
        $firstPart = substr($this->path, 0, strpos($this->path, "/-1"));
        $fuzzyMatchPart = null;
        
        
        foreach($batchParts as $batchPart){
            /*@var $batchPart BatchPart /
        
            if($batchPart->path == $firstPart){
                $fuzzyMatchPart = $batchPart;
                break;
            }
        }

        if($fuzzyMatchPart == NULL) throw new Exception("Matcher not found");

        $this->path = self::str_replace_first("-1", $fuzzyMatchPart->GetPayload()['id'], $this->path);
    }
*/
 /*   
    private function syncPayload(){
        foreach(array_keys($this->payload) as $key){
            //Ako item paylod-a sadrzi 'id' i vrijednost == -1 onda postavi sa linka
            if(strpos(strtolower($key), "id") !== false ){
            }
        }
    }
*/    
    
    public function PrepareJSON() {
        $prep = array('id' => $this->id,
                      'operation' => $this->operation,
                      'path' => $this->path,
                      'payload' => $this->payload != NULL? $this->payload : new \stdClass(),
                      'error' => $this->error,
                      'parentPartId' => $this->parentPartId );

        return $prep;
    }
    
    private static function str_replace_first($from, $to, $content){
        $from = '/'.preg_quote($from, '/').'/';

        return preg_replace($from, $to, $content, 1);
    }
    
    private static function str_replace_last($search, $replace, $subject){
        
        $pos = strrpos($subject, $search);

        if($pos !== false){
            $subject = substr_replace($subject, $replace, $pos, strlen($search));
        }

        return $subject;
    }
}


















/*

 
    private function buildPath(&$parts, &$part){

        $path = $part['path'];
        
        
        if(strpos($path, "-1") === false) return $path;
        
        $firstPart = substr($path, 0, strpos($path, "/-1"));
        $fuzzyMatchPart = null;
        
        foreach($parts as $p){
            
            if($p['path'] == $firstPart){// && $p['path'] != $part['path']){
                $fuzzyMatchPart = $p;
                break;
            }
            
        }
        
        //var_dump($fuzzyMatchPart);
        
        echo  $fuzzyMatchPart['payload']['id']. " " . $firstPart . PHP_EOL;
        
        if($fuzzyMatchPart == NULL) return $path;

        $path = self::str_replace_first("-1", $fuzzyMatchPart['payload']['id'], $path);
                
        //echo $path . PHP_EOL;
          
        $part['path'] = $path;
        //var_dump($part);
                
                
        
        
        //echo substr($path, 0, $firstPart) . 0 . substr($path, $firstPart+2, strlen($path)) . PHP_EOL;
        
        //echo PHP_EOL . ($path) . PHP_EOL;
        
        //$currPayload = $part['payload'];
        
        //while(strpos($path, "-1") !== false){
            
            //$path = 
            
        //}
        
        
        return $this->buildPath($parts, $part);
    }    
 
   */