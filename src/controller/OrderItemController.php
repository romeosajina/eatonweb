<?php

namespace controller;

use controller\BaseController;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use db\DBOrderItem as DBOrderItem;
use model\OrderItem as OrderItem;
use \shared\QueryOptions as QueryOptions;

class OrderItemController extends BaseController {

    public function getAll(Request $request, Response $response, array $args) {

        $queryOptions = $this->buildQueryOptions($request);

        $all = DBOrderItem::GetAll($this->db, $queryOptions);

        self::buildAllWithQueryParams($this->db, $all, $this->getQueryParams(), $queryOptions);

        $response->getBody()->write(OrderItem::AllToJSON($all));

        return $response;
    }

    public function get($request, $response, array $args) {

        $queryOptions = $this->buildQueryOptions($request);

        $item = DBOrderItem::GetById($this->db, $queryOptions);

        if ($item != null) {
            
            self::buildWithQueryParams($this->db, $item, $this->getQueryParams(), $queryOptions);
            $response->getBody()->write($item->ToJSON());
        
        } else {
        
            $response->getBody()->write("{}");

        }

        return $response;
    }

    public function add($request, $response, array $args) {

        $id = $request->getParam("id");
        $orderId = $request->getAttribute("orderId");
        
        $menuItemId = $request->getParam("menuItemId");
        $menuItemOptionId = $request->getParam("menuItemOptionId");
        $amount = $request->getParam("amount");
        $discount = $request->getParam("discount");
        $remark = $request->getParam("remark");

        $menuItemName = $request->getParam('menuItemName'); 
        $menuItemPrice = $request->getParam('menuItemPrice'); 
        $menuItemPhoto = $request->getParam('menuItemPhoto');
        $menuItemDiscount = $request->getParam('menuItemDiscount'); 
        $menuItemDescription = $request->getParam('menuItemDescription'); 
        
        $menuItemOptionName = $request->getParam("menuItemOptionName");
        $menuItemOptionPrice = $request->getParam("menuItemOptionPrice");

        $item = new OrderItem($id, $orderId, $menuItemId, $menuItemOptionId, $amount, $discount, $remark, $menuItemName, $menuItemPrice, $menuItemPhoto, $menuItemDiscount, $menuItemDescription, $menuItemOptionName, $menuItemOptionPrice);

        DBOrderItem::Save($this->db, $item, $this->user);

        $response->getBody()->write($item->ToJSON());

        return $response;
    }

    public function update($request, $response, array $args) {

        $id = $request->getAttribute("id");

        $orderId = $request->getParam("orderId");
        $menuItemId = $request->getParam("menuItemId");
        $menuItemOptionId = $request->getParam("menuItemOptionId");
        $amount = $request->getParam("amount");
        $discount = $request->getParam("discount");
        $remark = $request->getParam("remark");

        $menuItemName = $request->getParam('menuItemName'); 
        $menuItemPrice = $request->getParam('menuItemPrice'); 
        $menuItemPhoto = $request->getParam('menuItemPhoto');
        $menuItemDiscount = $request->getParam('menuItemDiscount'); 
        $menuItemDescription = $request->getParam('menuItemDescription'); 
        
        $menuItemOptionName = $request->getParam("menuItemOptionName");
        $menuItemOptionPrice = $request->getParam("menuItemOptionPrice");

        $item = new OrderItem($id, $orderId, $menuItemId, $menuItemOptionId, $amount, $discount, $remark, $menuItemName, $menuItemPrice, $menuItemPhoto, $menuItemDiscount, $menuItemDescription, $menuItemOptionName, $menuItemOptionPrice);

        DBOrderItem::Update($this->db, $item, $this->user);

        $response->getBody()->write($item->ToJSON());

        return $response;
    }

    public function delete($request, $response, array $args) {

        DBOrderItem::Delete($this->db, $this->buildQueryOptions($request), $this->user);

        $response->getBody()->write("{}");

        return $response;
    }

    public static function buildWithQueryParams($db, OrderItem $orderItem, $qryParms, QueryOptions $queryOptions) {

        if (self::isCollectionExpanded("orderItemSupplements", $qryParms)) {

            $qryParms = self::removeCollectionExpanded("orderItemSupplements", $qryParms);

            $queryOptions->SetOrderItemId($orderItem->GetId());

            $orderItem->SetOrderItemSupplement(\db\DBOrderItemSupplement::GetAll($db, $queryOptions));
        
            if (OrderItemSupplementController::needToPropagateBuild($qryParms)) {
                OrderItemSupplementController::buildAllWithQueryParams($db, $orderItem->GetOrderItemSupplements(), $qryParms, $queryOptions);
            }
        }
        
    }
    public static function needToPropagateBuild($qryParms) {
        return self::isCollectionExpanded("orderItemSupplements", $qryParms);
    }
}
