<?php

namespace controller;

use controller\BaseController;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \db\DBStatistics as DBStatistics;
use \model\Statistics as Statistics;
use \shared\QueryOptions as QueryOptions;

class StatisticsController extends BaseController {

    public function getAnnually(Request $request, Response $response, array $args) {

        $queryOptions = $this->buildQueryOptions($request);

        $queryOptions->SetCompanyId($this->user->GetId());

        $all = DBStatistics::GetAnnually($this->db, $queryOptions);

        $body = NULL;
        
        if(isset($request->getQueryParams()['includeSummary'])){
             
            $summary = DBStatistics::GetSummary($this->db, $queryOptions)->PrepareJSON();
            
            $summary['annually'] = Statistics::AllToArray($all);
            
            $body = (new Statistics($summary))->ToJSON();
            
        }else{
            $body = Statistics::AllToJSON($all);
        }
        
        $response->getBody()->write($body);

        return $response;
    }
    
    
    public function getMonthlyProducts(Request $request, Response $response, array $args) {

        $queryOptions = $this->buildQueryOptions($request);

        $queryOptions->SetCompanyId($this->user->GetId());

        $all = DBStatistics::GetMonthlyProducts($this->db, $queryOptions);

        $response->getBody()->write(Statistics::AllToJSON($all));

        return $response;
    }
    
}
