<?php

namespace controller;

use controller\BaseController;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use db\DBOrderItemSupplement as DBOrderItemSupplement;
use model\OrderItemSupplement as OrderItemSupplement;
use \shared\QueryOptions as QueryOptions;

class OrderItemSupplementController extends BaseController {

    public function getAll(Request $request, Response $response, array $args) {

        $queryOptions = $this->buildQueryOptions($request);

        $all = DBOrderItemSupplement::GetAll($this->db, $queryOptions);

        //self::buildAllWithQueryParams($this->db, $all, $this->getQueryParams(), $queryOptions);

        $response->getBody()->write(OrderItemSupplement::AllToJSON($all));

        return $response;
    }

    public function get($request, $response, array $args) {

        //$id = $request->getAttribute("id");

        $queryOptions = $this->buildQueryOptions($request);

        $item = DBOrderItemSupplement::GetById($this->db, $queryOptions);

        if ($item != null) {
            //self::buildWithQueryParams($this->db, $item, $this->getQueryParams(), $queryOptions);
            $response->getBody()->write($item->ToJSON());
        } else {
            $response->getBody()->write("{}");
        }

        return $response;
    }

    public function add($request, $response, array $args) {

        $id = $request->getParam("id");
        $orderItemId = $request->getAttribute("orderItemId");//$request->getParam("orderItemId");
          
        //$menuItemSupplementId = $request->getParam("menuItemSupplementId");
        //Ovo je Robert inszitira da se mora zvat tako jer ne znan zas
        $menuItemSupplementId = $request->getParam("menuItemOptionSupplementId");        
        $menuItemSupplementName = $request->getParam("menuItemOptionSupplementName");
        $menuItemSupplementPrice = $request->getParam("menuItemOptionSupplementPrice");

        $item = new OrderItemSupplement($id, $orderItemId, $menuItemSupplementId, $menuItemSupplementName, $menuItemSupplementPrice);

        DBOrderItemSupplement::Save($this->db, $item, $this->user);

        $response->getBody()->write($item->ToJSON());

        return $response;
    }

    public function update($request, $response, array $args) {

        $id = $request->getAttribute("id");

        $orderItemId = $request->getParam("orderItemId");
        $menuItemSupplementId = $request->getParam("menuItemOptionSupplementId");
        $menuItemSupplementName = $request->getParam("menuItemOptionSupplementName");
        $menuItemSupplementPrice = $request->getParam("menuItemOptionSupplementPrice");

        $item = new OrderItemSupplement($id, $orderItemId, $menuItemSupplementId, $menuItemSupplementName, $menuItemSupplementPrice);

        DBOrderItemSupplement::Update($this->db, $item, $this->user);

        $response->getBody()->write($item->ToJSON());

        return $response;
    }

    public function delete($request, $response, array $args) {

        DBOrderItemSupplement::Delete($this->db, $this->buildQueryOptions($request), $this->user);

        $response->getBody()->write("{}");

        return $response;
    }

    public static function buildWithQueryParams($db, OrderItemSupplement $orderItemSupplement, $qryParms, QueryOptions $queryOptions) {

    }

    public static function needToPropagateBuild($qryParms) {
        return false; //self::isCollectionExpanded("", $qryParms);
    }

}
