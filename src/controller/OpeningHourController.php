<?php

namespace controller;

use controller\BaseController;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use db\DBOpeningHour as DBOpeningHour;
use model\OpeningHour as OpeningHour;
use \shared\QueryOptions as QueryOptions;

class OpeningHourController extends BaseController {

    public function getAll(Request $request, Response $response, array $args) {

        $queryOptions = $this->buildQueryOptions($request);

        $all = DBOpeningHour::GetAll($this->db, $queryOptions);

        $response->getBody()->write(OpeningHour::AllToJSON($all));

        return $response;
    }

    public function get($request, $response, array $args) {

        $queryOptions = $this->buildQueryOptions($request);

        $item = DBOpeningHour::GetById($this->db, $queryOptions);

        if ($item != null) {
            $response->getBody()->write($item->ToJSON());
        } else {
            $response->getBody()->write("{}");
        }

        return $response;
    }

    public function add($request, $response, array $args) {

        $id = $request->getParam("id");
        $companyId = $request->getAttribute("companyId");//$request->getParam("companyId");
        $day = $request->getParam("day");
        $from = $request->getParam("from");
        $to = $request->getParam("to");

        $item = new OpeningHour($id, $companyId, $day, $from, $to);

        DBOpeningHour::Save($this->db, $item, $this->user);

        $response->getBody()->write($item->ToJSON());

        return $response;
    }

    public function update($request, $response, array $args) {

        $id = $request->getAttribute("id");

        $companyId = $request->getParam("companyId");
        $day = $request->getParam("day");
        $from = $request->getParam("from");
        $to = $request->getParam("to");

        $item = new OpeningHour($id, $companyId, $day, $from, $to);

        DBOpeningHour::Update($this->db, $item, $this->user);

        $response->getBody()->write($item->ToJSON());

        return $response;
    }

    public function delete($request, $response, array $args) {

        DBOpeningHour::Delete($this->db, $this->buildQueryOptions($request), $this->user);

        $response->getBody()->write("{}");

        return $response;
    }

    public static function buildWithQueryParams($db, OpeningHour $openingHour, $qryParms, QueryOptions $queryOptions) {}
    
    public static function needToPropagateBuild($qryParms) { return false; }
}
