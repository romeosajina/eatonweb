<?php

namespace controller;

use controller\BaseController;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use db\DBUser as DBUser; 
use db\DBCompany as DBCompany;
use model\User as User;
use model\Company as Company;


class AuthController extends BaseController{
    
    public function __construct(\Slim\Container $container) {
        $this->container = $container;
        $this->initDB();
    }
    

  
    public function auth(Request $request, Response $response, array $args) {

        $email = $request->getParam("email");
        $password = $request->getParam("password");

        $user = DBUser::Authenticate($this->db, $email, $password);
        
        if($user != null)
            $response->getBody()->write($user->ToJSON());

        else
            $response->getBody()->write('{}');
        
        return $response;
    }


    public function auth2(Request $request, Response $response, array $args) {

        $email = $request->getParam("email");
        $password = $request->getParam("password");
                
        $company = DBCompany::Authenticate($this->db, $email, $password);
        
        if($company != null)
            $response->getBody()->write($company->ToJSON());

        else
            $response->getBody()->write('{}');
        
        return $response;
    }
    
}