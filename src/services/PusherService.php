<?php

namespace services;
use Pusher\Pusher as Pusher;

class PusherService {
    
    const OPTIONS = array(
       'cluster' => 'eu',
       'encrypted' => true
    );
    
    const ON_CHANGED_EVENT = "on-changed-event";
    const ON_ADDED_EVENT   = "on-added-event";
    const ON_DELETED_EVENT = "on-deleted-event";
    
    //const USER_ORDERS_CHANNEL = "user-orders-channel";
    //const COMPANY_ORDERS_CHANNEL = "company-orders-channel";
    const USER_ORDERS_CHANNEL = "user-orders-channel-";
    const COMPANY_ORDERS_CHANNEL = "company-orders-channel-";
    
    
    
    /**
     * @var string
     */
    private $isInBatchMode = FALSE;
    /**
     * @var array
     */
    private $events = NULL;
    
    
    /**
     * @var Pusher
     */
    private static $instance = NULL;
    
    /**
     * @var PusherService
     */
    private static $serviceInstance = NULL;

    
    public static function SetInBatchMode(){ self::GetServiceInstance()->isInBatchMode = TRUE; }
    private function Add($channel, $event, $data){
        if($this->events == NULL)
            $this->events = array();
        
        array_push($this->events, new PusherEvent($channel, $event, $data));
    }


    /**
     * @var Pusher
     */
    private static function GetInstance(){
        
        if(self::$instance == NULL){
            self::$instance = new Pusher(
                                         'c0a53cea58f28c26b294',
                                         '9c4ca593ce3a84537682',
                                         '518841',
                                         self::OPTIONS
                                        );
        
            self::$instance->set_logger(new MyLogger());
            
            self::$serviceInstance = new PusherService();
        }
        
        return self::$instance;
    }
    
    /**
     * @var PusherService
     */
    private static function GetServiceInstance(){
        
        if(self::$serviceInstance == NULL){        
            self::$serviceInstance = new PusherService();
        }
        
        return self::$serviceInstance;
    }
    
    private static function Broadcast($channel, $event, $data){
        
        if(self::GetServiceInstance()->isInBatchMode)
            self::GetServiceInstance()->Add($channel, $event, $data);
            
        else
            $o = self::GetInstance()->trigger($channel, $event, $data, null, true);
        

        //var_dump($o);
    }
    
    public static function BroadcastAll(){
        if(!self::GetServiceInstance()->isInBatchMode){
            throw new \Exception("Cannot call BroadcastAll if not in batch mode!");
        }
        
        //Fix jer se u Broadcast provjerava da li je u batch modu
        self::GetServiceInstance()->isInBatchMode = FALSE;
        
        foreach (self::GetServiceInstance()->events as $pe) {
            /* @var $pe PusherEvent */
            
            self::Broadcast($pe->channel, $pe->event, $pe->data);
        }
        
        self::Clear();
    }
    
    public static function Clear(){
       self::GetServiceInstance()->events = NULL;
    }

    public static function OrderUpdated(\model\Order $order){
        //self::Broadcast(self::USER_ORDERS_CHANNEL, self::ON_CHANGED_EVENT, $order->PrepareJSON());
        self::Broadcast(self::USER_ORDERS_CHANNEL . $order->GetUserId(), self::ON_CHANGED_EVENT, $order->PrepareJSON());
    }
    
    public static function OrderAdded(\model\Order $order){
        //self::Broadcast(self::COMPANY_ORDERS_CHANNEL, self::ON_ADDED_EVENT, $order->PrepareJSON());
        self::Broadcast(self::COMPANY_ORDERS_CHANNEL . $order->GetCompanyId(), self::ON_ADDED_EVENT, $order->PrepareJSON());
    }
    
    public static function OrderDeleted(\model\Order $order){
        //self::Broadcast(self::COMPANY_ORDERS_CHANNEL, self::ON_DELETED_EVENT, $order->PrepareJSON());
        self::Broadcast(self::COMPANY_ORDERS_CHANNEL . $order->GetCompanyId(), self::ON_DELETED_EVENT, $order->PrepareJSON());
    }
        
}


class PusherEvent {
    
    public $channel;
    public $event;
    public $data;
    
    public function __construct($c, $e, $d) {
        $this->channel = $c;
        $this->event = $e;
        $this->data = $d;
    }
}

class MyLogger {
    public function log( $msg ) {
      //print_r( $msg . "\n" );
    }
}
